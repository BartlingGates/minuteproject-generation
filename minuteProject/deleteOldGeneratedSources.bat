set rootTargetPath=..\GeneratedSource\src\
set rootTargetApp=%rootTargetPath%main\java\com\company\model\
set rootTargetTest=%rootTargetPath%test\java\com\company\model\
set rootTargetJs=%rootTargetPath%js\com\company\model\angularJS\model\

rd /Q /S "%rootTargetApp%domain"
rd /Q /S "%rootTargetApp%dto"
rd /Q /S "%rootTargetApp%mapper"

rd /Q /S "%rootTargetApp%dao\face\"
rd /Q /S "%rootTargetApp%dao\impl\jpa\"
rd /Q /S "%rootTargetApp%dao\impl\"
rd /Q /S "%rootTargetApp%dao\"
rd /Q /S "%rootTargetApp%service\face\"
rd /Q /S "%rootTargetApp%service\impl\"
rd /Q /S "%rootTargetApp%service\"

rd /Q /S "%rootTargetApp%repository\"
rd /Q /S "%rootTargetApp%rest\"
rd /Q /S "%rootTargetApp%security\"
del /Q "%rootTargetPath%main\java\com\company\common\component\ClientIdChecker.java"

del /Q "%rootTargetPath%main\resources\import.sql"
del /Q "%rootTargetPath%main\java\com\company\common\component\CustomMapperRegistration.java"

del /Q "%rootTargetTest%config\TestModelURL.java"
rd /Q /S "%rootTargetTest%dao\"

rd /Q /S "%rootTargetTest%domain\"
rd /Q /S "%rootTargetTest%mapper\"
rd /Q /S "%rootTargetTest%rest\"

del /Q "%rootTargetPath%test\java\com\company\model\util\TestModelObjectFactory.java"

rd /Q /S "%rootTargetJs%"