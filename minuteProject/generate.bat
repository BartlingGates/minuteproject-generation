call deleteOldGeneratedSources.bat
call mywork\config\model-generation-CI-modified.cmd mywork\config mywork\config\mp-config-prototype.xml
if exist ChangeCommentsInImportSQLFile.class (
    REM .java file already compiled nothing to do.
) else (
    "%JAVA_HOME%\bin\javac" ChangeCommentsInImportSQLFile.java
)
"%JAVA_HOME%\bin\java" ChangeCommentsInImportSQLFile ../GeneratedSource/src/main/resources/import.sql
pause