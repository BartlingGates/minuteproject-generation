import java.io.*;
import java.util.*;

public class ChangeCommentsInImportSQLFile {

	public static void main(String[] args) throws Exception {

		String[] lines = readLines(args[0]);
		FileWriter fw = new FileWriter(args[0]);
		boolean multiLineComment = false;
		for (String line : lines) {
			if (line.startsWith("/*")) {
				multiLineComment = true;
			}
			if (multiLineComment) {
				line = "-- " + line;
			}
			if (line.endsWith("*/")) {
				multiLineComment = false;
			}
			fw.write(line+"\r\n");
		}
		fw.close();
	}

	private static String[] readLines(String filename) throws IOException {
		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> lines = new ArrayList<String>();
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			lines.add(line);
		}
		bufferedReader.close();
		return lines.toArray(new String[lines.size()]);
	}
}