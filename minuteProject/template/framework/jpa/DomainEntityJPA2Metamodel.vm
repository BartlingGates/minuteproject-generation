#exposeVariableEntitySpecific()
#exposeEnvironmentParams()
package $packageName;

import java.sql.*;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SetAttribute;
import java.util.HashSet;
import java.util.Set;

@StaticMetamodel(${domainClassName}.class)
public class $className {

#if ($tableUtils.isCompositePrimaryKeyNotMany2Many($table))
    public static volatile SingularAttribute<${domainClassName}, $embeddedIdClass> $embeddedIdVariable;
#else
#foreach($column in ${table.primaryKeyColumns})
#putColumnParams()
#columnJavaNaming()
    public static volatile SingularAttribute<${domainClassName}, $columnType> $columnVar;
#end
#end

#foreach($column in ${table.attributes})
#putColumnParams()
#columnJavaNaming()
#if($isEnum)
#set ($enumColumn = $columnUtils.getColumn($table, $column.name))
#set ($columnType = $enumUtils.getEnumType($template, $enumColumn))
#end
    public static volatile SingularAttribute<${domainClassName}, $columnType> $columnVar;
#end
#if ($isEmbedded)
#foreach($column in ${table.primaryKeyColumns})
#putColumnParamNaming()
#if(!$isForeignKey)
#set ($enumColumn = $columnUtils.getColumn($table, $column.name))
#set ($columnType = $enumUtils.getEnumType($template, $enumColumn))
    public static volatile SingularAttribute<${domainClassName}, $columnType> ${columnVar}_;
#end
#end
#end
#foreach ($reference in $table.parents)
#putReferenceParams2()
#set($colVar = $commonUtils.getColumnAliasVariable($table, $reference))
    public static volatile SingularAttribute<${domainClassName}, $linkedTableClass> $colVar;
#set($column = $reference.localColumn)
#putColumnParamNaming()
    public static volatile SingularAttribute<${domainClassName}, $columnType> ${colVar}_;
#end

#foreach ($reference in $table.children)
#putReferenceParams2()
#if(!$linktableDB.isManyToMany())
    public static volatile SetAttribute<${domainClassName}, $linkedTableClass> ${childrenListVariable};
#end
#end

#foreach ($linkReference in $enrichmentUtils.getLinkedTargetReferenceByMany2Many($table))
#exposeM2MSpecific()
    public static volatile SetAttribute<${domainClassName}, $targetTableName> ${childrenListVariable};
#end

#foreach($column in ${table.primaryKeyColumns})
#putColumnParamNaming()
    public static Class<?> ${columnVar}JavaType() {
    	return ${columnVar}.getJavaType();
    }

#end
#foreach($column in ${table.attributes})
#putColumnParams()
#columnJavaNaming()
	public static Class<?> ${columnVar}JavaType() {
		return ${columnVar}.getJavaType();
	}

#end
#foreach ($reference in $table.children)
#putReferenceParams2()
    public static Class<?> ${childrenListVariable}JavaType() {
        return ${childrenListVariable}.getBindableJavaType();
    }

#end
#foreach ($reference in $table.parents)
#putReferenceParams2()
#set($colVar = $commonUtils.getColumnAliasVariable($table, $reference))
    public static Class<?> ${colVar}JavaType() {
        return ${colVar}.getBindableJavaType();
    }
#set($colVar = $commonUtils.getColumnAliasVariable($table, $reference))
#putColumnParamNaming()
    
    public static Class<?> ${colVar}_JavaType() {
        return ${colVar}_.getJavaType();
    }

#end
    public static Set<SingularAttribute<${domainClassName}, ?>> getEntitySingularAttributes() {
        Set<SingularAttribute<${domainClassName}, ?>> result = new HashSet<>();
#foreach ($reference in $table.parents)
#putReferenceParams2()
#set($colVar = $commonUtils.getColumnAliasVariable($table, $reference))
        result.add($colVar);
#end
        return result;
    }

    public static Set<SetAttribute<${domainClassName}, ?>> getAllSetAttributes() {
        Set<SetAttribute<${domainClassName}, ?>> result = new HashSet<>();
#foreach ($reference in $table.children)
#putReferenceParams2()
#if(!$linktableDB.isManyToMany())
    result.add(${childrenListVariable});
#end
#end
        return result;
    }

}
