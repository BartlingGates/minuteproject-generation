#exposeVariableModelSpecific()
package $packageName;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.data.domain.Pageable;

import com.company.model.domain.ApplicationCode;

import net.sf.minuteProject.architecture.bsla.domain.AbstractDomainObject;

public abstract class $className <T extends AbstractDomainObject> {

	protected class ResultTotalAndList {
		public final List<Object[]> list;
		public final Integer total;
		ResultTotalAndList(Integer total, List<Object[]> list) {
			this.total = total;
			this.list = list;
		}
	}

    @PersistenceContext (unitName = "default")
    protected EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

	protected ResultTotalAndList loadPaginatedBySQLQuery(Pageable pageable, Long clientId, String[] projection,
			String filterExpression, String orderBy, Map<String, Object> valuesMap) throws ClassNotFoundException {
        Session s = entityManager.unwrap(Session.class);

    	String[] tableNames = new String[projection.length];

    	String outerJoin = "";
    	List<String> projectionList = new ArrayList<>();
    	List<String> alias = new ArrayList<>();
    	String where = " WHERE ";
    	for (int i=0; i < projection.length; i++) {
    		Class<?> c = Class.forName(ApplicationCode.class.getPackage().getName() + "." +projection[i].trim());
    		alias.add(c.getSimpleName());
    		Table[] t = c.getAnnotationsByType(Table.class);
    		tableNames[i] = t[0].name();
    		projectionList.add(alias.get(i) +".id"+ " as " + alias.get(i) + "_id");

    		if (StringUtils.isEmpty(outerJoin)) {
    			outerJoin = tableNames[i] +" "+ alias.get(i);
    			where += alias.get(i) + ".client_id=:clientId ";
    		} else {
    			outerJoin += " LEFT JOIN " + tableNames[i] + " " + alias.get(i) + " ON " + alias.get(i-1)+".id=" + alias.get(i)+"."+tableNames[i-1]+"_id AND " + alias.get(i) + ".client_id=:clientId ";
    		}
    	}

    	String expression = filterExpression == null ? "" : " AND " + filterExpression;

		String selectCount = "SELECT COUNT(1) "//
				+ " FROM " + outerJoin + where + expression;
		SQLQuery countQuery = s.createSQLQuery(selectCount);
    	setSQLParameter(clientId, valuesMap, countQuery);

    	BigInteger totalBigInt = (BigInteger) countQuery.uniqueResult();
		Integer total = totalBigInt.intValue();

		String select = "SELECT " + String.join(",", projectionList) //
				+ " FROM " + outerJoin + where + expression;
		if (StringUtils.isNotEmpty(orderBy)) {
			select += " ORDER BY " + orderBy;
		}

		SQLQuery query = s.createSQLQuery(select);
    	setSQLParameter(clientId, valuesMap, query);

    	query.setFirstResult( pageable.getOffset() );
        query.setFetchSize( pageable.getPageSize() );
        query.setMaxResults( pageable.getPageSize() );

    	@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
        return new ResultTotalAndList(total, list);
    }

	private void setSQLParameter(Long clientId, Map<String, Object> valuesMap, SQLQuery query) {
		query.setLong("clientId", clientId);
    	if (valuesMap != null) {
    		for (String key : valuesMap.keySet()) {
    			Object o = valuesMap.get(key);
    			query.setParameter(key, o);
    		}
    	}
	}

}