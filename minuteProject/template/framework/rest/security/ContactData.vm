		if (${tableVariableName}.getParentType().equalsIgnoreCase("site")) {
            // Standortmanagement
            if (${tableVariableName}.getSiteId_() != null) {
                Site site = siteDao.loadSite(${tableVariableName}.getSiteId_());
                if (site != null) {
                    if (site.getParentType().equalsIgnoreCase("contractor")) {
                        if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, site.getContractorId_(), site.getContractorId())) {
                            possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.SITE_MANAGEMENT.toString());
                        } else {
                            throw new AccessDeniedException(SecurityErrorMessageConstants.WRONG_CONTRACTOR.toString());
                        }
                    } else if (site.getParentType().equalsIgnoreCase("businessPartner")) {
                        if (site.getBusinessPartnerId().getContractorId() != null) {
                            if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, site.getBusinessPartnerId().getContractorId_(), site.getBusinessPartnerId().getContractorId())) {
                                possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.BUSINESS_PARTNER_MANAGEMENT.toString());
                            }
                        } else if (site.getBusinessPartnerId().getAddressBookEntryId() != null) {
                            AddressBookEntry addressBookEntry = site.getBusinessPartnerId().getAddressBookEntryId();    
                            if (addressBookEntry.getAddressBookId() != null) {
                                if (addressBookEntry.getAddressBookId().getParentType().equalsIgnoreCase("portaluser")) {
                                    if (authenticationReader.isAssignedPortalUserAuthenticated(authenticatedPortalUserId, addressBookEntry.getAddressBookId().getPortalUserId_(), addressBookEntry.getAddressBookId().getPortalUserId())) {
                                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_PRIVATE.toString());
                                    }
                                } else if (addressBookEntry.getAddressBookId().getParentType().equalsIgnoreCase("contractor")) {
                                    if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, addressBookEntry.getAddressBookId().getContractorId_(), addressBookEntry.getAddressBookId().getContractorId())) {
                                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_CONTRACTOR.toString());
                                    }
                                }   
                            }                        
                        }
                    }
                    
                } else {
                    throw new IllegalArgumentException(SecurityErrorMessageConstants.ASSIGNED_NOT_EXISTING.toString());     
                }
            } else {
                throw new IllegalArgumentException(SecurityErrorMessageConstants.NO_PARENT_TYPE_MATCH.toString());
            }
        } else if (${tableVariableName}.getParentType().equalsIgnoreCase("contractor")) {
            // Firmenangaben bzw. Firmenprofil
            if (${tableVariableName}.getContractorId_() != null) {
                Contractor contractor = contractorDao.loadContractor(${tableVariableName}.getContractorId_());
                if (contractor != null) {
                    if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, contractor.getId(), contractor)) {
                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.COMPANY_DATA.toString());
                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.COMPANY_PROFILE.toString());
                    } else {
                        throw new AccessDeniedException(SecurityErrorMessageConstants.WRONG_CONTRACTOR.toString());    
                    }
                } else {
                    throw new IllegalArgumentException(SecurityErrorMessageConstants.ASSIGNED_NOT_EXISTING.toString());     
                }
            } else {
                throw new IllegalArgumentException(SecurityErrorMessageConstants.NO_PARENT_TYPE_MATCH.toString());
            }
        } else if (${tableVariableName}.getParentType().equalsIgnoreCase("person")) {
            // Benutzerverwaltung oder eigenes Profil
            if (${tableVariableName}.getPersonId_() != null) {
                Person person = personDao.loadPerson(${tableVariableName}.getPersonId_());
                if (person != null) {
                	if(person.getPortalUserId() != null){
	                    if (authenticationReader.isAssignedPortalUserAuthenticated(authenticatedPortalUserId, person.getPortalUserId().getId(), person.getPortalUserId())) {
	                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.OWN_USER_PROFILE.toString());
	                    }
	                    if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, person.getPortalUserId().getContractorId().getId(), person.getPortalUserId().getContractorId())) {
	                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.USER_MANAGEMENT.toString());        
	                    }
                	}

                	if(person.getSiteId() != null){
                        if (person.getClientId().equals(authenticatedClientId)) {
                            if (person.getSiteId().getContractorId() != null) {
                                possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.SITE_MANAGEMENT.toString());
                            } else if (person.getSiteId().getBusinessPartnerId() != null) {
                                possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.BUSINESS_PARTNER_MANAGEMENT.toString());
                            }
                        }
	                        
                	}
                	
					if(person.getAddressBookEntryId() != null){
						if(person.getAddressBookEntryId().getAddressBookId().getPortalUserId() != null){
	                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_PRIVATE.toString());
						}
						
						if(person.getAddressBookEntryId().getAddressBookId().getContractorId() != null){
	                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_CONTRACTOR.toString());
						}
                	}
					
                    return possibleTechnicalFeatureNames;
                } else {
                    throw new IllegalArgumentException(SecurityErrorMessageConstants.ASSIGNED_NOT_EXISTING.toString());   
                }
            } else {
                throw new IllegalArgumentException(SecurityErrorMessageConstants.NO_PARENT_TYPE_MATCH.toString());
            }
        } else if (${tableVariableName}.getParentType().equalsIgnoreCase("businessPartner")) {
            if (${tableVariableName}.getBusinessPartnerId_() != null) {
                BusinessPartner businessPartner = businessPartnerDao.loadBusinessPartner(${tableVariableName}.getBusinessPartnerId_());
                if (businessPartner != null) {
					if (businessPartner.getClientId().equals(authenticatedClientId)){
                        if (businessPartner.getContractorId() != null) {
                            possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.BUSINESS_PARTNER_MANAGEMENT.toString());    
                            return possibleTechnicalFeatureNames;
                        } else if (businessPartner.getAddressBookEntryId() != null) {
                            AddressBookEntry addressBookEntry = businessPartner.getAddressBookEntryId();    
                            if (addressBookEntry.getAddressBookId() != null) {
                                if (addressBookEntry.getAddressBookId().getParentType().equalsIgnoreCase("portaluser")) {
                                    if (authenticationReader.isAssignedPortalUserAuthenticated(authenticatedPortalUserId, addressBookEntry.getAddressBookId().getPortalUserId_(), addressBookEntry.getAddressBookId().getPortalUserId())) {
                                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_PRIVATE.toString());
                                    }
                                } else if (addressBookEntry.getAddressBookId().getParentType().equalsIgnoreCase("contractor")) {
                                    if (authenticationReader.isAssignedContractorAuthenticated(authenticatedContractorId, addressBookEntry.getAddressBookId().getContractorId_(), addressBookEntry.getAddressBookId().getContractorId())) {
                                        possibleTechnicalFeatureNames.add(TechnicalFeatureConstants.ADDRESSBOOK_CONTRACTOR.toString());
                                    }
                                }   
                            }
                        } 
                    } else {
                        throw new AccessDeniedException(SecurityErrorMessageConstants.WRONG_CONTRACTOR.toString());    
                    }
                } else {
                    throw new IllegalArgumentException(SecurityErrorMessageConstants.ASSIGNED_NOT_EXISTING.toString());     
                }
            } else {
                throw new IllegalArgumentException(SecurityErrorMessageConstants.NO_PARENT_TYPE_MATCH.toString());
            }
        }
        
        return possibleTechnicalFeatureNames;
