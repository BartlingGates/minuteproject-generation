/*
	 Copyright (c) minuteproject, minuteproject@gmail.com
	 All rights reserved.
	 
	 Licensed under the Apache License, Version 2.0 (the "License")
	 you may not use this file except in compliance with the License.
	 You may obtain a copy of the License at
	 
	 http://www.apache.org/licenses/LICENSE-2.0
	 
	 Unless required by applicable law or agreed to in writing, software
	 distributed under the License is distributed on an "AS IS" BASIS,
	 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 See the License for the specific language governing permissions and
	 limitations under the License.
	 
	 More information on minuteproject:
	 twitter @minuteproject
	 wiki http://minuteproject.wikispaces.com 
	 blog http://minuteproject.blogspot.net
	 
*/
/*
	 template reference : 
	 - Minuteproject version : 0.9.8
	 - name      : application_codes
	 - file name : sql_imports/application_codes.vm
*/

INSERT INTO application_code(client_id, code, created_by, created_date, description, type_key, language, last_modified_by, last_modified_date, domain, sort_order) VALUES (0, 'Status', 'company', CURRENT_TIMESTAMP, N'ORD', N'ordered', 'de', 'company', CURRENT_TIMESTAMP, 'Contract', 0);
INSERT INTO application_code(client_id, code, created_by, created_date, description, type_key, language, last_modified_by, last_modified_date, domain, sort_order) VALUES (0, 'Status', 'company', CURRENT_TIMESTAMP, N'CON', N'contracted', 'de', 'company', CURRENT_TIMESTAMP, 'Contract', 2);
INSERT INTO application_code(client_id, code, created_by, created_date, description, type_key, language, last_modified_by, last_modified_date, domain, sort_order) VALUES (0, 'Status', 'company', CURRENT_TIMESTAMP, N'DEC', N'declined', 'de', 'company', CURRENT_TIMESTAMP, 'Contract', 3);
INSERT INTO application_code(client_id, code, created_by, created_date, description, type_key, language, last_modified_by, last_modified_date, domain, sort_order) VALUES (0, 'Status', 'company', CURRENT_TIMESTAMP, N'REJ', N'rejectetd', 'de', 'company', CURRENT_TIMESTAMP, 'Contract', 1);
