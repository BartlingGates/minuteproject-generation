package tool;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class StringEncrypterAndDecrypter extends JFrame implements ClipboardOwner {

	private static final long serialVersionUID = 1L;

	private final static StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();

	JTextField textfield0 = new JTextField(50);
	JTextField textfield1 = new JTextField(50);

	public StringEncrypterAndDecrypter() {
		encryptor.setPassword("Password");

		setTitle("StringEncrypterAndDecrypter");
		textfield1.setEditable(false);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(textfield0, BorderLayout.NORTH);
		getContentPane().add(textfield1, BorderLayout.SOUTH);
		pack();
		setLocationRelativeTo(null);

		textfield0.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				String text0 = textfield0.getText();
				if (text0.startsWith("ENC(") && text0.endsWith(")")) {
					text0 = text0.substring(4, text0.length() - 1);
				}
				String text1 = null;
				if (text0 != null && text0.length() > 3) {
					try {
						text1 = decrypt(text0);
					} catch (Exception exp) {
					}
					if (text1 == null) {
						text1 = encrypt(text0);
					}
				}
				if (text1 != null) {
					textfield1.setText(text1);
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(new StringSelection(text1), StringEncrypterAndDecrypter.this);
				} else {
					textfield1.setText("");
				}
			}
		});

		String clipboard = getClipboardContents();
		if (clipboard != null) {
			textfield0.setText(clipboard);
		}
	}

	public static String encrypt(String s) {
		return encryptor.encrypt(s);
	}

	public static String decrypt(String s) {
		return encryptor.decrypt(s);
	}

	public static void main(String[] args) {
		StringEncrypterAndDecrypter f = new StringEncrypterAndDecrypter();

		System.out.println(encrypt("4711081"));
		System.out.println(encrypt("47110815"));
		System.out.println(encrypt("47110815"));
		System.out.println(encrypt("47110815"));
		System.out.println(decrypt("66aWeHy0Y1MTfauBpkdmHjP81jIU3yoh"));
		System.out.println(encrypt("1111"));
		System.out.println(decrypt("cJOVcaH9ZwJWt8WUzJQang=="));

		f.setVisible(true);
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		// do nothing
	}

	public String getClipboardContents() {
		String result = null;
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String) contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException | IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

}
