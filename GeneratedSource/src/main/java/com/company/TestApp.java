package com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;


/**
 * Created by Christian on 17.11.2015.
 */

@SpringBootApplication
@ActiveProfiles("test")
public class TestApp {
//public class TestApp implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(TestApp.class, args);
    }

    public void run(String... strings) throws Exception {

    	 System.out.println("Running... ;-)");
    	 
    }
}
