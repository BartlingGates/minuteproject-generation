package com.company;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    @Bean
    public Docket coreImpl() {                
        return new Docket(DocumentationType.SWAGGER_2)
		.directModelSubstitute(LocalDate.class, String.class)
		.directModelSubstitute(LocalDateTime.class, java.util.Date.class)
		.groupName("Core-API")
		.select()
		.apis(RequestHandlerSelectors.basePackage("com.company.model.rest"))
		.paths(PathSelectors.ant("/**"))
		.build()
		.apiInfo(apiInfo())
		.useDefaultResponseMessages(false)
		.ignoredParameterTypes(OAuth2Authentication.class); //wird als Parameter benötigt, um auf Authentifizierung zugreifen zu können & wird durch Spring Boot gefüllt
    }
     
    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
          "REST API",
          "PROTOTYPE Middleware API.",
          "API TOS",
          "Terms of service",
          "my_address@company.com",
          "License of API",
          "API license URL");
        return apiInfo;
    }
 }