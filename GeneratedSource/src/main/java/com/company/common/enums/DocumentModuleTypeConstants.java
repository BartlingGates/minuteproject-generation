package com.company.common.enums;

public enum DocumentModuleTypeConstants {

    
    FILE("file"),
    BUSINESS_LOGO("businesslogo"),
    SUBJECT("subject"),
    CONTENT("content"),
    FOOTER("footer");
    
    
    private final String name;

    private DocumentModuleTypeConstants(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public static DocumentModuleTypeConstants getEnumByName(String name) {
        for (DocumentModuleTypeConstants type : DocumentModuleTypeConstants.values()) {
            if (type.toString().equals(name.toLowerCase())) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given module type is not valid");
    }
}
