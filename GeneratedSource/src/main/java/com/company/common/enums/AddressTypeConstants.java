package com.company.common.enums;

public enum AddressTypeConstants {
    PERSONAL("personal"),
    BUSINESS("business"),
    INVOICE("invoice"),
    SUPPLIER("supplier"),
    CUSTOMER("customer"),
    COMPANY("company"),
    SITE("site");
    
    
    private final String name;

    private AddressTypeConstants(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public static AddressTypeConstants getEnumByName(String name) {
        for (AddressTypeConstants type : AddressTypeConstants.values()) {
            if (type.toString().equals(name.toLowerCase())) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given address type is not valid");
    }
}
