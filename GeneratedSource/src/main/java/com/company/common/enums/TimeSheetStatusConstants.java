package com.company.common.enums;

import java.io.Serializable;

public enum TimeSheetStatusConstants implements Serializable  {
    
    PLANNED("PLANNED"),
    TRACKED("TRACKED");

    private final String type;

    TimeSheetStatusConstants(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
    
    public static TimeSheetStatusConstants getEnumByName(String name) {
        for (TimeSheetStatusConstants type : TimeSheetStatusConstants.values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given timesheet status is not valid");
    }
}
