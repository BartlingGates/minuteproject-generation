package com.company.common.enums;

import java.io.Serializable;

public enum FilterCostants implements Serializable {
	FIELDNAME,
	OPERATOR,
	VALUE;
}
