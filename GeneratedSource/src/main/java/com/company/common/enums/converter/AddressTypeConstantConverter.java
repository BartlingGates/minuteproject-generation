package com.company.common.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.company.common.enums.AddressTypeConstants;

@Converter
public class AddressTypeConstantConverter implements AttributeConverter<AddressTypeConstants, String>{

    @Override
    public String convertToDatabaseColumn(AddressTypeConstants attribute) {
        if (attribute != null) {
            return attribute.toString();
        }
        return null;
    }

    @Override
    public AddressTypeConstants convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return AddressTypeConstants.getEnumByName(dbData);
        }
        return null;
    }
}
