package com.company.common.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.company.common.enums.TimeSheetStatusConstants;

@Converter
public class TimeSheetStatusConstantConverter implements AttributeConverter<TimeSheetStatusConstants, String>{

    @Override
    public String convertToDatabaseColumn(TimeSheetStatusConstants attribute) {
        if (attribute != null) {
            return attribute.toString();
        }
        return null;
    }

    @Override
    public TimeSheetStatusConstants convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return TimeSheetStatusConstants.getEnumByName(dbData);
        }
        return null;
    }
}
