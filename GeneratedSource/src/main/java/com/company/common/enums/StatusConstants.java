package com.company.common.enums;

import java.io.Serializable;

public enum StatusConstants implements Serializable  {
    
    UNLICENSED("unlicensed"),
	CONFIRMED("confirmed"),
    DEACTIVATED("deactivated"),
    ACTIVATED("activated");

    private final String type;

    StatusConstants(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
