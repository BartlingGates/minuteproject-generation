package com.company.common.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.company.common.enums.CarrierTypeConstants;

@Converter
public class CarrierTypeConstantConverter implements AttributeConverter<CarrierTypeConstants, String> {

	@Override
	public String convertToDatabaseColumn(CarrierTypeConstants attribute) {
		if (attribute != null) {
			return attribute.toString();
		}
		return null;
	}

	@Override
	public CarrierTypeConstants convertToEntityAttribute(String dbData) {
		if (dbData != null) {
			return CarrierTypeConstants.getEnumByName(dbData);
		}
		return null;
	}
}
