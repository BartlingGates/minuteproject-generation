package com.company.common.enums;

public enum SiteModel {

	ID(0),
	LOCATION_NAME(1),
	LOCATION_SHORT_NAME(2),
	PARENT_SITE_ID(3),
	PARENT_TYPE(4),
	SITE_LEVEL(5);
	
	public int value;
	 
	private SiteModel(int value) {
		this.value = value;
	}
}
