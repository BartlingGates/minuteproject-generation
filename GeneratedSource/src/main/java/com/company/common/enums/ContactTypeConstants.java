package com.company.common.enums;

import java.io.Serializable;

public enum ContactTypeConstants implements Serializable {
	EMAIL("email"),
	MOBILE("mobile"),
	TELEPHONE("telephone"),
	FAX("fax"),
	TWITTER("twitter"),
	SKYPE("skype"),
	CHANNEL_SMS("SMS"),
	CHANNEL_MAIL("Mail"),
	HOMEPAGE("homepage");
	
	private final String name;

    private ContactTypeConstants(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
