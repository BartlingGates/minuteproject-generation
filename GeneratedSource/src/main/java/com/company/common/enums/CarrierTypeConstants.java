package com.company.common.enums;

public enum CarrierTypeConstants {
    NONE("none"), // no carrier
    DHL("dhl"), // DHL
    DPD("dpd"), // Deutscher Paket Dienst
    // unused DHLS("dhls"), // DHL Sperrgut
    FEDEX("fedex"), // Fed Ex
    GLS("gls"),
    HERMES("hermes"), // Hermes Logistik Gruppe
    // unused TNT("TNT"), // TNT
    UPS("ups"); // United Parcel Service
	
	private final String name;

    private CarrierTypeConstants(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public static CarrierTypeConstants getEnumByName(String name) {
        for (CarrierTypeConstants type : CarrierTypeConstants.values()) {
            if (type.toString().equals(name.toLowerCase())) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given carrier type is not valid");
    }
}
