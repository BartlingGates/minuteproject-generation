package com.company.common.enums;

import java.io.Serializable;

public enum ServiceOrderTypeConstants implements Serializable  {
    
    MAINTENANCE("MAINTENANCE"),
    SERVICE("SERVICE");

    private final String type;

    ServiceOrderTypeConstants(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
    
    public static ServiceOrderTypeConstants getEnumByName(String name) {
        for (ServiceOrderTypeConstants type : ServiceOrderTypeConstants.values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given service order type is not valid");
    }
}
