package com.company.common.enums;

public enum PlantModel {

	ID(0),
	PLANT_LEVEL(1),
	PLANT_NAME(2),
	PLANT_NUMBER(3),
	PLANT_SITE_ID(4),
	PLANT_CREATED_BY(5);
	
	public int value;
	 
	private PlantModel(int value) {
		this.value = value;
	}
}
