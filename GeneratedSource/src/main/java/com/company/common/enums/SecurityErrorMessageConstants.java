package com.company.common.enums;

import java.io.Serializable;

public enum SecurityErrorMessageConstants implements Serializable  {
     
    CANNOT_FIND_FEATURE("Cannot find feature for given context");

    private final String message;

    SecurityErrorMessageConstants(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
