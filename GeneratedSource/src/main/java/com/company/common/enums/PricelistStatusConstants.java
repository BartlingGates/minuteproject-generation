package com.company.common.enums;

import java.io.Serializable;

public enum PricelistStatusConstants implements Serializable  {
    
    PLANNED("PLANNED"),
    TRACKED("TRACKED");

    private final String type;

    PricelistStatusConstants(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
    
    public static PricelistStatusConstants getEnumByName(String name) {
        for (PricelistStatusConstants type : PricelistStatusConstants.values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Given pricelist status is not valid");
    }
}
