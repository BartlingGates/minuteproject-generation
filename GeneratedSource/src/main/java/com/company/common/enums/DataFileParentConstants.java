package com.company.common.enums;

public enum DataFileParentConstants {
    ATTACHMENT("attachment", "attachment"),
    EQUIPMENT("equipment", "equipment"),
    SERVICE_BOOKING_ACTIVITY("service_booking_activity", "serviceBookingActivity"),
    SERVICE_INQUIRY_ACTIVITY("service_inquiry_activity", "serviceInquiryActivity"),
    SERVICE_OFFER_ACTIVITY("service_offer_activity", "serviceOfferActivity"),
    SERVICE_ORDER_ACTIVITY("service_order_activity", "serviceOrderActivity"),
    SERVICE_POOL_ACTIVITY("service_pool_activity", "servicePoolActivity"),
    SERVICE_REPORT_ACTIVITY("service_report_activity", "serviceReportActivity"),
    SERVICE_WORK_ORDER_ACTIVITY("service_work_order_activity", "serviceWorkArderActivity"),
    SITE("site", "site"),
    WORKLIST_DETAIL("worklist_detail", "worklistDetail");
    
    private final String tableName;
    private final String variableName;

    private DataFileParentConstants(final String tableName, final String variableName) {
        this.tableName = tableName;
        this.variableName = variableName;
    }

    @Override
    public String toString() {
        return tableName;
    }
    
    public String getVariableName() {
        return variableName;
    }
    
    public String getClassName() {
        return variableName.substring(0, 1).toUpperCase() + variableName.substring(1);
    }
    
    public String getTableName() {
        return tableName;
    }
    
    public boolean isParentType(String name) {
        return name.toLowerCase().equals(tableName.toLowerCase()) || name.toLowerCase().equals(variableName.toLowerCase());
    }
}
