package com.company.common.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.company.common.enums.PricelistStatusConstants;

@Converter
public class PricelistStatusConstantConverter implements AttributeConverter<PricelistStatusConstants, String>{

    @Override
    public String convertToDatabaseColumn(PricelistStatusConstants attribute) {
        if (attribute != null) {
            return attribute.toString();
        }
        return null;
    }

    @Override
    public PricelistStatusConstants convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return PricelistStatusConstants.getEnumByName(dbData);
        }
        return null;
    }
}
