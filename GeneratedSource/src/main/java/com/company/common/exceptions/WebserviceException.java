package com.company.common.exceptions;

import com.mashape.unirest.http.HttpResponse;

public class WebserviceException extends Exception {

	private static final long serialVersionUID = 7208896801513287706L;

	private HttpResponse<?> httpResponse;
	
	public WebserviceException(HttpResponse<String> httpResponse) {
		this.httpResponse = httpResponse;
	}

	public HttpResponse<?> getHttpResponse() {
		return httpResponse;
	}
	
}
