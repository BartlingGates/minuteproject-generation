package com.company.common.component;

import org.springframework.stereotype.Component;

import com.company.model.domain.Customer;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Component
public class CustomerIdMapper extends BidirectionalConverter<Long, Customer> {

	  @Override
	  public Customer convertTo(Long source, Type<Customer> destinationType) {
	    return new Customer();
	  }

	  @Override
	  public Long convertFrom(Customer source, Type<Long> destinationType) {
	    return source.getId();
	  }
	}