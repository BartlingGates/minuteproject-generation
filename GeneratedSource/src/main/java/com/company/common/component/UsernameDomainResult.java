package com.company.common.component;

public class UsernameDomainResult {
    
    private String username;
    private String subdomain;
    
    public UsernameDomainResult(String username, String subdomain) {
        this.username = username;
        this.subdomain = subdomain;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String domain) {
        this.subdomain = domain;
    }
    
    public boolean isSubdomainPresent() {
        return subdomain != null;
    }
}
