package com.company.common.component;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Component
public class LocalDateConverter extends BidirectionalConverter<LocalDate, LocalDate> {

    @Override
    public LocalDate convertTo(LocalDate source, Type<LocalDate> destinationType) {
        return LocalDate.from(source);
    }

    @Override
    public LocalDate convertFrom(LocalDate source, Type<LocalDate> destinationType) {
        return LocalDate.from(source);
    }

}
