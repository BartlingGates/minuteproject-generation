package com.company.common.component;

import org.apache.commons.lang3.StringUtils;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class MappingContextFactory {

    public static final String CURRENT_LAYERS = "currentLayers";
    public static final String MAX_LAYERS = "maxLayers";
    public static final String EXCLUDE = "exclude";
    public static final String EXCLUDE_SEPARATOR = ",";

    private static final int MAX_LAYERS_ALL = -1;


    public static MappingContext getMappingContext(int maxLayers) {
        return getMappingContext(0, maxLayers);
    }

    public static MappingContext getMappingContext(int currentLayers, int maxLayers) {
        return getMappingContext(currentLayers, maxLayers, null);
    }

    public static MappingContext getMappingContext(String... excludeObjects) {
        return getMappingContext(0, MAX_LAYERS_ALL, excludeObjects);
    }

    public static MappingContext getMappingContext(int currentLayers, int maxLayers, String[] excludeObjects) {
    	MappingContext.Factory mappingContextFactory = new MappingContext.Factory();

		final DefaultMapperFactory.Builder builder = new DefaultMapperFactory.Builder();
		builder.mappingContextFactory(mappingContextFactory).build();

        MappingContext context = mappingContextFactory.getContext();
        context.setProperty(CURRENT_LAYERS, currentLayers);
        context.setProperty(MAX_LAYERS, maxLayers);

        if (excludeObjects == null) {
            excludeObjects = new String[0];
        }
        context.setProperty(EXCLUDE, StringUtils.join(excludeObjects, ","));

        return context;
    }

}
