package com.company.common.component;

public final class UsernameDomainSplitter {
    
    private static final String USERNAME_DOMAIN_SEPARATOR = "@";
    
    private UsernameDomainSplitter() {
        
    }
    
    /**
     * Splits the input into username and domain
     * Sets username and domain to given values.
     * @param input concatenated username and domain, seperated by specific character
     * @return true if domain is present, else false
     */
    public static UsernameDomainResult getUsernameAndDomain(String input) {
        String subdomain = null;
        String username = null; 
        
        if (input != null) {
            if (input.endsWith(USERNAME_DOMAIN_SEPARATOR)) {
                username = input.substring(0, input.length() - 1);
            } else if (!input.contains("@")) {
                username = input;
            } else {
                int lastIndexOfAt = input.lastIndexOf("@");
                username = input.substring(0, lastIndexOfAt);
                subdomain = input.substring(lastIndexOfAt + 1, input.length());
            }
        }
        
        return new UsernameDomainResult(username, subdomain);
    }

}
