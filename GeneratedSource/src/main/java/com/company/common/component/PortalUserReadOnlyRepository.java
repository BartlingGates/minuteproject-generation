package com.company.common.component;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.company.model.domain.PortalUser;

public interface PortalUserReadOnlyRepository extends Repository<PortalUser, Long> {
    
public Long countByUsernameIgnoreCase(@Param("username") String username);
    
    @Query("SELECT COUNT(p) FROM PortalUser p JOIN p.customerId c"
            + " WHERE lower(p.username) = lower(:username)"
            + " AND (c.subdomain IS NULL OR lower(c.subdomain) = lower(:subdomain))")
    public Long countByUsername(@Param("username") String username, @Param("subdomain") String subdomain);

}
