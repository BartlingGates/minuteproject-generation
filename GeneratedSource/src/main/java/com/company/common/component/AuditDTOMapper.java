package com.company.common.component;

import org.springframework.stereotype.Component;

import com.company.model.domain.face.ModelAbstractEntity;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import util.AuditDTO;

@Component
public class AuditDTOMapper extends CustomMapper<ModelAbstractEntity, AuditDTO>{


    @Override
    public void mapAtoB(ModelAbstractEntity source, AuditDTO target, MappingContext context) {
        target.setClientId(source.getClientId());
        target.setCreatedBy(source.getCreatedBy());
        target.setCreatedDate(source.getCreatedDate());
        target.setId(source.getId());
        target.setLastModifiedBy(source.getLastModifiedBy());
        target.setLastModifiedDate(source.getLastModifiedDate());
        
    }
}
