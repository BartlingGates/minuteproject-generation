package com.company.common.component;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by kanswa on 16.11.2016.
 */
@Component
public class SpringContext implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static String  getEnvironmentProperty(String key) {
        if(applicationContext == null)
            return  null;

        if(applicationContext.getEnvironment() == null)
            return null;

        return applicationContext.getEnvironment().getProperty(key);
    }
}
