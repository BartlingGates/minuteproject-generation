package com.company.common.component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.company.common.enums.DocumentModuleTypeConstants;

@Converter
public class DocumentModuleTypeConstantConverter implements AttributeConverter<DocumentModuleTypeConstants, String>{

    @Override
    public String convertToDatabaseColumn(DocumentModuleTypeConstants attribute) {
        if (attribute != null) {
            return attribute.toString();
        }
        return null;
    }

    @Override
    public DocumentModuleTypeConstants convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return DocumentModuleTypeConstants.getEnumByName(dbData);
        }
        return null;
    }
}
