package com.company.common.component;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerBaseErrorHandler {

	/**
	 * Behandelt die Übergabe von ungültigen Argumenten (z.B. Parameter ID stimmt bei PUT nicht mit ID in Entity überein)
	 * und setzt den Status der Antwort auf BAD REQUEST
	 * @param ex
	 * @param response
	 * @throws IOException
	 */
	@ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException ex, HttpServletResponse response)  throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
	
	/**
	 * Behandelt Fehler beim Parsen von übergebenen Daten (z.B. ungültiges Objekt oder falscher Datentyp eines Attributs)
	 * und setzt den Status der Antwort auf BAD REQUEST
	 * @param ex
	 * @param response
	 * @throws IOException
	 */
	@ExceptionHandler
	void handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletResponse response) throws IOException {
		String message = "";
		if (ex.getCause() != null) {
			message = ex.getCause().getMessage();
		}
		response.sendError(HttpStatus.BAD_REQUEST.value(), "Error parsing request data: " + message);
	}
}
