package com.company.common.component;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Created by timme04 on 07.10.2014.
 */
@SuppressWarnings("rawtypes")
@Component
public class MapperFacadeFactory implements FactoryBean<MapperFacade>, ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Autowired
	private CustomMapperRegistration customMapperRegistration;
	
	@Autowired
	private CoreMapperRegistration coreMapperRegistration;

	@SuppressWarnings("unchecked")
	public MapperFacade getObject() throws Exception {
		final DefaultMapperFactory.Builder builder = new DefaultMapperFactory.Builder();
		final DefaultMapperFactory factory = builder.useAutoMapping(false).build();
		
		coreMapperRegistration.registerCoreMappers(factory);

		customMapperRegistration.registerCoreMappers(factory);

		final Map<String, BidirectionalConverter> converters = applicationContext
				.getBeansOfType(BidirectionalConverter.class);
		for (final BidirectionalConverter converter : converters.values()) {
			factory.getConverterFactory().registerConverter(converter);
		}

		return factory.getMapperFacade();
	}

	public Class<?> getObjectType() {
		return MapperFacade.class;
	}

	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
