package com.company.common.component;

import org.apache.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.company.model.dto.PortalUserDTO;
import com.company.model.dto.PortalUserPasswordDTO;

public class PortalUserPWEncryptor {
	
	public static Logger log = Logger.getLogger(PortalUserPWEncryptor.class.getName());
	
	public static PortalUserPasswordDTO encryptPassword(PortalUserPasswordDTO portalUserPasswordDTO){
	
		try{
			
			portalUserPasswordDTO.setPassword(new BCryptPasswordEncoder().encode(portalUserPasswordDTO.getPassword()));
		}
		catch(Exception ex){
			log.error("Could not encrypt PW");
		}
		
		return portalUserPasswordDTO;
	}
	
	public static PortalUserDTO encryptPassword(PortalUserDTO portalUserDTO){
		
		try{
			
			portalUserDTO.setPassword(new BCryptPasswordEncoder().encode(portalUserDTO.getPassword()));
		}
		catch(Exception ex){
			log.error("Could not encrypt PW");
		}
		
		return portalUserDTO;
	}
	
	public static String encryptPassword(String password) {
	    try {
	        return new BCryptPasswordEncoder().encode(password);
	    } catch (Exception ex) {
	        log.error("Count not encrypt PW", ex);
	    }
	    return password;
	}
}
