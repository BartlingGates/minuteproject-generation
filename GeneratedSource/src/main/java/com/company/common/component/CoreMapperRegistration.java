package com.company.common.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.model.domain.face.ModelAbstractEntity;

import ma.glasnost.orika.impl.DefaultMapperFactory;
import util.AuditDTO;

@Component
public class CoreMapperRegistration {
    
    @Autowired
    private AuditDTOMapper auditDTOMapper;
    
    public void registerCoreMappers(DefaultMapperFactory factory) {
        factory.classMap(ModelAbstractEntity.class, AuditDTO.class)
            .exclude("clone")
            .customize(auditDTOMapper)
            .register();
    }

}
