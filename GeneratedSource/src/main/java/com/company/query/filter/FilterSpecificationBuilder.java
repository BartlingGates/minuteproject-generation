package com.company.query.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class FilterSpecificationBuilder<T> {

    private List<FilterCriteria> criterias;
    private Class<?> genericMetaModelClass;
    
    public FilterSpecificationBuilder(Class<?> genericMetaModelClass) {
        this.genericMetaModelClass = genericMetaModelClass;
        this.criterias = new ArrayList<FilterCriteria>();
    }
    
    public void add(String filter) {
        this.criterias.add(new FilterCriteria(filter, genericMetaModelClass));
    }
    
    public void add(String[] filters) {
        for (String filter : filters) {
            this.add(filter);
        }
    }
    
    public void add(FilterCriteria criteria) {
        if (criteria != null) {
            criterias.add(criteria);
        }
    }
    
    public void add(List<FilterCriteria> criterias) {
        if (criterias != null) {
            this.criterias.addAll(criterias);
        }
    }
    
    public void clear() {
        criterias.clear();
    }

    @Deprecated
    public Specification<T> build() {
        return build(FilterCriteria.LINKING_OPERATOR_AND);
    }
    
    public Specification<T> build(String linkingOperation) {
        Specification<T> result;
        
        if (criterias == null || criterias.size() == 0) {
            return null;
        }

        result = new FilterSpecificationImpl<T>(criterias, linkingOperation.toUpperCase());
        
        return result;
        
    }
    
    public Specification<T> build(String linkingOperation, Root<T> root, CriteriaBuilder builder) {
        Specification<T> result = null;
        
        return result;
    }
//    
//    public List<Predicate> buildPredicateList(CriteriaQuery<?> query, CriteriaBuilder cb, String linkingOperation) {
//        List<Predicate> predicates = new ArrayList<>();
//        CriteriaQuery<T> query = (CriteriaQuery<T>) cb.createQuery();
//        
//        return predicates;
//    }
    
    
}
