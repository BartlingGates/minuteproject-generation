package com.company.query.filter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class FilterSpecificationImpl<T> implements Specification<T> {
    
    public static final String EQUAL ="eq";
    public static final String NOT_EQUAL = "ne";
    public static final String GREATER_THAN = "gt";
    public static final String LESS_THAN = "lt";
    public static final String GREATER_OR_EQUAL_THAN = "ge";
    public static final String LESS_OR_EQUAL_THAN = "le";
    public static final String LIKE = "like";
    
    private List<FilterCriteria> criteriasOnRoot;
    Map<String, List<FilterCriteria>> criteriasJoined;
    private String linkingOperator;
    
    public FilterSpecificationImpl(List<FilterCriteria> criterias, String linkingOperator) {
        this.linkingOperator = linkingOperator;
        fillCriterias(criterias);
    }
    
    public FilterSpecificationImpl(FilterCriteria criteria) {
        this.linkingOperator = FilterCriteria.LINKING_OPERATOR_AND;
        List<FilterCriteria> criterias = new ArrayList<>();
        criterias.add(criteria);
        fillCriterias(criterias);
    }
    
   
    
    private void fillCriterias(List<FilterCriteria> criterias) {
        this.criteriasOnRoot = new ArrayList<>();
        this.criteriasJoined = new HashMap<>();
        for (FilterCriteria criteria : criterias) {
            if (criteria.getJoinedAttributeName() == null) {
                criteriasOnRoot.add(criteria);
            } else {
                List<FilterCriteria> list = new ArrayList<>();
                if (criteriasJoined.containsKey(criteria.getJoinedAttributeName())) {
                    list = criteriasJoined.get(criteria.getJoinedAttributeName());
                }
                list.add(criteria);
                criteriasJoined.put(criteria.getJoinedAttributeName(), list);
            }
        }
    }
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();
        if ((criteriasJoined != null) && (criteriasJoined.size() > 0)) {
            query.distinct(true);
        }
        if (criteriasJoined != null) {
			for (Entry<String, List<FilterCriteria>> entry : criteriasJoined.entrySet()) {
				Join join = root.join(entry.getKey(), JoinType.LEFT);
				for (FilterCriteria criteria : entry.getValue()) {
					if (criteria.getJavaTypeOfValue().equals(Long.class)) {
						predicates.add(getLongPredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(String.class)) {
						predicates.add(getStringPredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(LocalDateTime.class)) {
						predicates.add(getLocalDateTimePredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(LocalDate.class)) {
						predicates.add(getLocalDatePredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(Integer.class)) {
						predicates.add(getIntegerPredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(BigDecimal.class)) {
						predicates.add(getBigDecimalPredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(Double.class)) {
						predicates.add(getDoublePredicate(join, query, cb, criteria));
					} else if (criteria.getJavaTypeOfValue().equals(Boolean.class)
							|| criteria.getJavaTypeOfValue().equals(boolean.class)) {
						predicates.add(getBooleanPredicate(join, query, cb, criteria));
					} else if (Enum.class.isAssignableFrom(criteria.getJavaTypeOfValue())) {
						predicates.add(getEnumPredicate(join, query, cb, criteria));
					} else {
						throw new IllegalArgumentException("No query processing for datatype '"
								+ criteria.getJavaTypeOfValue().getName() + "' defined.");
					}
				}
			}
        }
        for (FilterCriteria criteria : criteriasOnRoot) {
            if (criteria.getJavaTypeOfValue().equals(Long.class)) {
                predicates.add(getLongPredicate(root, query, cb, criteria));
            } else if (criteria.getJavaTypeOfValue().equals(String.class)) {
                predicates.add(getStringPredicate(root, query, cb, criteria));
            } else if(criteria.getJavaTypeOfValue().equals(LocalDateTime.class)) {
                predicates.add(getLocalDateTimePredicate(root, query, cb, criteria));
            } else if (criteria.getJavaTypeOfValue().equals(LocalDate.class)) {
                predicates.add(getLocalDatePredicate(root, query, cb, criteria));
            } else if(criteria.getJavaTypeOfValue().equals(Integer.class)) {
                predicates.add(getIntegerPredicate(root, query, cb, criteria));
            } else if(criteria.getJavaTypeOfValue().equals(BigDecimal.class)) {
                predicates.add(getBigDecimalPredicate(root, query, cb, criteria));
            } else if(criteria.getJavaTypeOfValue().equals(Double.class)) {
                predicates.add(getDoublePredicate(root, query, cb, criteria));
            } else if(criteria.getJavaTypeOfValue().equals(Boolean.class) ||
                    criteria.getJavaTypeOfValue().equals(boolean.class)) {
                predicates.add(getBooleanPredicate(root, query, cb, criteria));
            } else if(Enum.class.isAssignableFrom(criteria.getJavaTypeOfValue())) {
                predicates.add(getEnumPredicate(root, query, cb, criteria));
            } else {
                throw new IllegalArgumentException("No query processing for datatype '" + criteria.getJavaTypeOfValue().getName() + "' defined.");
            }
        }
        
        if (linkingOperator == FilterCriteria.LINKING_OPERATOR_OR) {
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        } else {
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        }
    }
    
    private Predicate getLongPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty()) 
                return cb.isNull(root.<Long>get(criteria.getFieldName()));
            else 
                return cb.equal(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<Long>get(criteria.getFieldName()));
            else
                return cb.notEqual(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(root.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getLongPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty()) 
                return cb.isNull(join.<Long>get(criteria.getFieldName()));
            else 
                return cb.equal(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<Long>get(criteria.getFieldName()));
            else
                return cb.notEqual(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(join.<Long>get(criteria.getFieldName()), Long.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getStringPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(root.<String>get(criteria.getFieldName()));
            else
                return cb.equal(cb.lower(root.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<String>get(criteria.getFieldName()));
            else
                return cb.notEqual(cb.lower(root.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        case LIKE:
            return cb.like(cb.lower(root.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getStringPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(join.<String>get(criteria.getFieldName()));
            else
                return cb.equal(cb.lower(join.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<String>get(criteria.getFieldName()));
            else
                return cb.notEqual(cb.lower(join.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        case LIKE:
            return cb.like(cb.lower(join.<String>get(criteria.getFieldName())), criteria.getValue().toLowerCase());
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getLocalDateTimePredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(root.<Timestamp>get(criteria.getFieldName()));
            else
                return cb.equal(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<Timestamp>get(criteria.getFieldName()));
            else
                return cb.notEqual(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(root.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getLocalDateTimePredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(join.<Timestamp>get(criteria.getFieldName()));
            else
                return cb.equal(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<Timestamp>get(criteria.getFieldName()));
            else
                return cb.notEqual(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(join.<Timestamp>get(criteria.getFieldName()), Timestamp.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getLocalDatePredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        if (!criteria.getValue().contains(":") && !criteria.getValue().isEmpty()) {
            criteria.setValue(criteria.getValue() + " 00:00:00");
        }
        
        return getLocalDateTimePredicate(root, query, cb, criteria);
    }
    
    private Predicate getLocalDatePredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        if (!criteria.getValue().contains(":") && !criteria.getValue().isEmpty()) {
            criteria.setValue(criteria.getValue() + " 00:00:00");
        }
        
        return getLocalDateTimePredicate(join, query, cb, criteria);
    }
    
    private Predicate getIntegerPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(root.<Integer>get(criteria.getFieldName()));
            else
                return cb.equal(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<Integer>get(criteria.getFieldName()));
            else
                return cb.notEqual(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(root.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getIntegerPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(join.<Integer>get(criteria.getFieldName()));
            else
                return cb.equal(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<Integer>get(criteria.getFieldName()));
            else
                return cb.notEqual(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(join.<Integer>get(criteria.getFieldName()), Integer.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getBigDecimalPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(root.<BigDecimal>get(criteria.getFieldName()));
            else 
                return cb.equal(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<BigDecimal>get(criteria.getFieldName()));
            else
                return cb.notEqual(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case GREATER_THAN:
            return cb.greaterThan(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case LESS_THAN:
            return cb.lessThan(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(root.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getBigDecimalPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(join.<BigDecimal>get(criteria.getFieldName()));
            else 
                return cb.equal(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<BigDecimal>get(criteria.getFieldName()));
            else
                return cb.notEqual(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case GREATER_THAN:
            return cb.greaterThan(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case LESS_THAN:
            return cb.lessThan(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(join.<BigDecimal>get(criteria.getFieldName()), BigDecimal.valueOf(Double.valueOf(criteria.getValue())));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getDoublePredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(root.<Double>get(criteria.getFieldName()));
            else
                return cb.equal(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(root.<Double>get(criteria.getFieldName()));
            else
                return cb.notEqual(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(root.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getDoublePredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        switch (criteria.getOperator()) {
        case EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNull(join.<Double>get(criteria.getFieldName()));
            else
                return cb.equal(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case NOT_EQUAL:
            if (criteria.getValue().isEmpty())
                return cb.isNotNull(join.<Double>get(criteria.getFieldName()));
            else
                return cb.notEqual(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case GREATER_THAN:
            return cb.greaterThan(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case LESS_THAN:
            return cb.lessThan(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case GREATER_OR_EQUAL_THAN:
            return cb.greaterThanOrEqualTo(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        case LESS_OR_EQUAL_THAN:
            return cb.lessThanOrEqualTo(join.<Double>get(criteria.getFieldName()), Double.valueOf(criteria.getValue()));
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getBooleanPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        boolean value = (Integer.parseInt(criteria.getValue()) == 0) ? false : true;
        switch (criteria.getOperator()) {
        case EQUAL:
            return cb.equal(root.<Boolean>get(criteria.getFieldName()), value);
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getBooleanPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        boolean value = (Integer.parseInt(criteria.getValue()) == 0) ? false : true;
        switch (criteria.getOperator()) {
        case EQUAL:
            return cb.equal(join.<Boolean>get(criteria.getFieldName()), value);
        default:
            throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
        }
    }
    
    private Predicate getEnumPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        try {
            if (criteria.getValue().isEmpty()) {
                switch (criteria.getOperator()) {
                case EQUAL:
                    return cb.isNull(root.<String>get(criteria.getFieldName()));
                case NOT_EQUAL:
                    return cb.isNotNull(root.<String>get(criteria.getFieldName()));
                default:
                    throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
                }
            } else {
                Method getEnumByName = criteria.getJavaTypeOfValue().getMethod("getEnumByName", String.class);
                Enum<?> value = (Enum<?>) getEnumByName.invoke(null, criteria.getValue());
                switch (criteria.getOperator()) {
                case EQUAL:
                    return cb.equal(root.<String>get(criteria.getFieldName()), value);
                case NOT_EQUAL:
                    return cb.notEqual(root.<String>get(criteria.getFieldName()), value);
                default:
                    throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
                }
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Cannot create enum from given value");
        }

    }
    
    private Predicate getEnumPredicate(Join join, CriteriaQuery<?> query, CriteriaBuilder cb, FilterCriteria criteria) {
        try {
            if (criteria.getValue().isEmpty()) {
                switch (criteria.getOperator()) {
                case EQUAL:
                    return cb.isNull(join.<String>get(criteria.getFieldName()));
                case NOT_EQUAL:
                    return cb.isNotNull(join.<String>get(criteria.getFieldName()));
                default:
                    throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
                }
            } else {
                Method getEnumByName = criteria.getJavaTypeOfValue().getMethod("getEnumByName", String.class);
                Enum<?> value = (Enum<?>) getEnumByName.invoke(null, criteria.getValue());
                switch (criteria.getOperator()) {
                case EQUAL:
                    return cb.equal(join.<String>get(criteria.getFieldName()), value);
                case NOT_EQUAL:
                    return cb.notEqual(join.<String>get(criteria.getFieldName()), value);
                default:
                    throw new IllegalArgumentException("No query processing for datatype '" + criteria.getClass().getName() + "' and operator '" + criteria.getOperator() + "' defined.");
                }
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Cannot create enum from given value");
        }

    }
}
