package com.company.query.filter;

import java.lang.reflect.Method;

import com.company.common.enums.FilterCostants;

public class FilterCriteria {

    private static final String GET_TYPE_METHOD_SUFFIX = "JavaType";
    private static final String TABLE_FIELDNAME_SEPARATOR = ".";
    public static final String FILTER_COMPONENTS_SEPARATOR = ",";
    public static final String FILTER_SEPARATOR = ";";
    public static final String LINKING_OPERATOR_SEPERATOR = ":";
    public static final String LINKING_OPERATOR_AND = "AND";
    public static final String LINKING_OPERATOR_OR = "OR";
    

    private String fieldName;
    private String operator;
    private String value;
    private String joinedAttributeName;
    private Class<?> javaType;

    public FilterCriteria(String filter, Class<?> metaModelClass) {
        String tokenFilter[] = filter.split(FILTER_COMPONENTS_SEPARATOR);
        this.operator = tokenFilter[FilterCostants.OPERATOR.ordinal()];
        if (tokenFilter.length > 2) {
            this.value = tokenFilter[FilterCostants.VALUE.ordinal()];
        } else  {
            this.value = "";
        }
        String tmpFieldName = tokenFilter[FilterCostants.FIELDNAME.ordinal()];
        if (tmpFieldName.contains(TABLE_FIELDNAME_SEPARATOR)) {
            String tokenFieldname[] = tmpFieldName.split("\\" + TABLE_FIELDNAME_SEPARATOR);
            setJavaTypeAndJoinedClassMetaModel(tokenFieldname[0], tokenFieldname[1], metaModelClass);
        } else {
            setJavaType(tmpFieldName, metaModelClass);
        }
    }

    public FilterCriteria(String fieldName, String operator, String value,
            Class<?> metaModelClass) {
        this.fieldName = fieldName.trim();
        this.operator = operator.trim();
        this.value = value.trim();
        setJavaType(fieldName, metaModelClass);
    }
    
    private void setJavaType(String fieldName, Class<?> metaModelClass) {
        this.joinedAttributeName = null;
        this.fieldName = fieldName;
        try {
            Method meth = metaModelClass.getMethod(fieldName + GET_TYPE_METHOD_SUFFIX);
            this.javaType = (Class<?>) meth.invoke(this);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }
    
    private void setJavaTypeAndJoinedClassMetaModel(String joinedTableName, String fieldName, Class<?> metaModelClass) {
        try {
            Method meth = metaModelClass.getMethod(joinedTableName + GET_TYPE_METHOD_SUFFIX);
            Class<?> joinedClass = (Class<?>) meth.invoke(this);
            Class<?> joinedMetaModelClass = Class.forName(joinedClass.getName() + "_");
            meth = joinedMetaModelClass.getMethod(fieldName + GET_TYPE_METHOD_SUFFIX);
            this.javaType = (Class<?>) meth.invoke(this);
            this.joinedAttributeName = joinedTableName;
            this.fieldName = fieldName;
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getOperator() {
        return operator;
    }

    public String getValue() {
        return value;
    }

    public Class<?> getJavaTypeOfValue() {
        return javaType;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getJoinedAttributeName() {
        return joinedAttributeName;
    }
}
