package com.company.security;

public class RequiredResourceAuthorization {
	
	public static int getRequiredAuthorizationBits(String method) {
		int requiredBits = 0b0;
		switch (method) {
		case "POST":
			 requiredBits = 0b1000;
			 break;
		case "GET":
			 requiredBits = 0b0100;
			 break;
		case "PUT":
			 requiredBits = 0b0010;
			 break;
		case "DELETE":
			 requiredBits = 0b0001;
			 break;
		}
		return requiredBits;
	}
}
