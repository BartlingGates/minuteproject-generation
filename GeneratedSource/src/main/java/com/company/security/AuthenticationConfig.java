package com.company.security;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.company.model.domain.PortalUser;

/**
 * Konfiguration der Authentifizierungsmethode.
 *   
 * @author Janna Wieneke
 * 
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Configuration
public class AuthenticationConfig extends GlobalAuthenticationConfigurerAdapter{
	
	@Autowired
	private PortalUserDetailsService users;
	
	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(users);
	}
}

/**
 * Service zum Auslesen und Prüfen der Anmeldedaten
 * 
 * @author Janna Wieneke
 * 
 */
@Service
class PortalUserDetailsService implements UserDetailsService {
	
	private static String FIND_BY_USERNAME = "SELECT p FROM PortalUser p WHERE p.username LIKE :username";
	
	@Autowired
	@PersistenceContext (unitName = "default")
    private EntityManager entityManager;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		TypedQuery<PortalUser> query = entityManager.createQuery(FIND_BY_USERNAME, PortalUser.class);
		query.setParameter("username", username);
		
		PortalUser currentUserdata = null;
		try {
		currentUserdata = query.getSingleResult();
		} catch (NoResultException | NonUniqueResultException ex) {
			throw new UsernameNotFoundException("Problem with username '" 
								+ username + "': " + ex.getMessage());
		}
		if (currentUserdata.getDeleted() == Boolean.TRUE) {
			throw new UsernameNotFoundException("Account for user '" 
								+ username + "' is not valid.");
		}
		
		// Authorisierungen werden über Interceptor geprüft, deshalb hier leere Liste verweden
		List<GrantedAuthority> auth = AuthorityUtils.
				commaSeparatedStringToAuthorityList("");
		String password = currentUserdata.getPassword();
		return new User(username, password, auth);
	}
}