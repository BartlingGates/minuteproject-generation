package com.company.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
/**
 * Konfiguration für die Verwendung von Auditing-Informationen (zur Verwendung von Annotation wie @CreatedDate, @CreatedBy etc.).
 * @author Janna Wieneke
 *
 */
@EnableJpaAuditing
@Configuration
public class AuditingConfiguration {

	@Bean
	public AuditorAware<String> createAuditorProvider()  {
		return new SecurityAuditor();
	}
	
	@Bean
	public AuditingEntityListener createAuditingEventListener() {
		return new AuditingEntityListener();
	}
	
	/**
	 * Liefert den Benutzernamen, der als Auditor in der Datenbank verwendet werden soll zurück.
	 * @author Janna Wieneke
	 *
	 */
	public static class SecurityAuditor implements AuditorAware<String> {

		@Override
		public String getCurrentAuditor() {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth == null) {
				return "unauthenticated user";
			} else {
				return auth.getName();
			}
		}
		 
	}
}
