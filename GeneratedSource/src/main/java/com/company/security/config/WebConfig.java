package com.company.security.config;
/**
 * Konfiguration zur Definition von Interceptorn.
 * 
 * @author Janna Wieneke
 * 
 */
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.company.security.interceptor.AuthorizationInterceptor;



@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(authorizationInterceptor())
	    	// Interceptor für Fehler ignorieren
	    	.excludePathPatterns("/error")
	    	// Interceptor für alle Ressourcen, die keine Authentifizierung erfordern ignorieren
	    	.excludePathPatterns(BaseResourceServerConfiguration.getPermitAllPattern());
	} 
	
	/**
	 * Liefert einen Interceptor zur Überprüfung von Authorisierungen zurück.
	 * @return Interceptor zur Überprüfung von Authorisierung
	 */
	@Bean
	public AuthorizationInterceptor authorizationInterceptor() {
		return new AuthorizationInterceptor();
	}
}
