package com.company.security.config;

public enum AdditionalAuthInfo {
	
	CUSTOMER_ID("customer_id"),
	AUTHORIZATION("authorization");
	
	private String key;
	
	AdditionalAuthInfo(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}

}
