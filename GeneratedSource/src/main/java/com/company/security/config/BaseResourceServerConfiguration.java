package com.company.security.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
/**
 * Konfiguration des Resource-Servers.
 * Es werden mithilfe von Pattern Ressourcen definiert, die (k)eine Authentifizierung erfordern.
 * 
 * @author Janna Wieneke
 *
 */
@Configuration
@EnableResourceServer
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class BaseResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	
	private static Set<String> permitAllPatterns;
	
	public BaseResourceServerConfiguration() {
        addPermitAllPattern("/swagger-ui.html");
        addPermitAllPattern("/webjars/springfox-swagger-ui/**");
        addPermitAllPattern("/swagger-resources/**");
        addPermitAllPattern("/v2/api-docs/**");
        addPermitAllPattern("/v2/api-docs*");
        addPermitAllPattern("/manage/**");
	}
	
	public static void addPermitAllPattern(String... pattern) {
		if (permitAllPatterns == null) {
			permitAllPatterns = new HashSet<String>();
		}
		for (String str : pattern) {
			permitAllPatterns.add(str);
		}
	}
	
	public static String[] getPermitAllPattern() {
		if (permitAllPatterns == null) {
			return new String[0];
		}
		return permitAllPatterns.toArray(new String[permitAllPatterns.size()]);
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests() 
			.antMatchers(getPermitAllPattern()).permitAll()
			.antMatchers("/**").authenticated();
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}