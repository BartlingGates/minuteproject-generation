package com.company.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.company.security.config.AdditionalAuthInfo;

@Configuration
@EnableAuthorizationServer 
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

  @Autowired
//  @Qualifier("authenticationManagerBean")
  private AuthenticationManager authenticationManager;
  
 
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.authenticationManager(authenticationManager);
    //endpoints.tokenServices(tokenServices);
    endpoints.tokenEnhancer(tokenEnhancer());
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
        .withClient("vsm")
        .secret("vsmsecret")
        .authorizedGrantTypes("refresh_token", "password")
        .scopes("company-application")
        // Ablaufdauer auf eine Stunde setzen
        .accessTokenValiditySeconds(60 * 60)
        // Ablaufdauer Refresh Token auf 14 Tage setzen
        .refreshTokenValiditySeconds(60 * 60 * 24 * 14);
  }
  
  @Bean
  public TokenEnhancer tokenEnhancer() {
	  return new CustomTokenEnhancer();
  }
  
}

class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken,
			OAuth2Authentication authentication) {
		User user = (User) authentication.getPrincipal();
		final Map<String, Object> additionalInfo = new HashMap<>();
		additionalInfo.put(AdditionalAuthInfo.CUSTOMER_ID.getKey(), "my-customer-id (ADDITIONAL)");
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		
		return accessToken;
	}
	
}