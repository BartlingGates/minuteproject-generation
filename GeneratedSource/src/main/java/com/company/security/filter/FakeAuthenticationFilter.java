package com.company.security.filter;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class FakeAuthenticationFilter implements Filter {
	
	public static final String OAUTH_CLIENT_ID_SECRET = "dnNtOnZzbXNlY3JldA==";
	
	@Value("${security.oauth2.resource.tokenLoginURL}")
	String LOGIN_URL;
	@Value("${security.oauth2.resource.tokenUsername}")
	String USERNAME;
	@Value("${security.oauth2.resource.tokenPassword}")
	String PASSWORD;
	
	 private Logger log = Logger.getLogger(this.getClass());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		HttpServletRequest request = (HttpServletRequest) req;
		
		
		if (request.getHeader("referer") != null
				&& request.getHeader("referer").contains("swagger")
				&& !request.getRequestURI().contains("swagger")
				&& !request.getRequestURI().contains("api-docs")
				&& !request.getRequestURI().contains("configuration")) {
			
			log.debug("SKIP SWAGGER: Skip Authentication for Swagger");
			
			try {
				String queryString = request.getQueryString();
				StringBuffer requestURL = request.getRequestURL();
				String fullRequestURL = requestURL.toString();
				
				if (queryString != null) {
			        fullRequestURL = requestURL.append('?').append(queryString).toString();
			    }
				
				HttpResponse<String> skippedAuthenticationResponse = getResponseForSwagger(fullRequestURL,
					request.getMethod(), request.getHeader("accept"), request.getContentType(),
					getBody(request));
				response.setStatus(skippedAuthenticationResponse.getStatus());
				response.getWriter().write(skippedAuthenticationResponse.getBody());
				response.getWriter().flush();
				response.getWriter().close();
				response.setHeader("content-type", request.getHeader("accept"));
			} catch (KeyManagementException | NoSuchAlgorithmException | JSONException
					| UnirestException e) {
				// TODO Auto-generated catch block
				log.debug("Cannot skip authentication for swagger request.");
				e.printStackTrace();
			}
			
			
		}
		else {
			chain.doFilter(request, response);
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
	
	public String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}
	
	private HttpResponse<String> getResponseForSwagger(String url, String method, String acceptType, String contentType, String requestBody) throws JSONException, KeyManagementException, NoSuchAlgorithmException, UnirestException {
		
		// Werden aus properties geladen
		//final String LOGIN_URL = "https://localhost:8090/oauth/token";
		//final String USERNAME = "swagger-user";
		//final String PASSWORD = "swagger-password";
		String accessToken;
		CloseableHttpClient httpClient;
		
		// Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
 
//        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        
        httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(allHostsValid)
                .setSSLContext(sc)
                .setConnectionTimeToLive(3000000, TimeUnit.MILLISECONDS)
                .build();
        
        Unirest.setHttpClient(httpClient);
        
        HttpResponse<JsonNode> response = Unirest.post(LOGIN_URL)
                .header("Authorization", "Basic " + OAUTH_CLIENT_ID_SECRET)
                .field("username", USERNAME)
                .field("password", PASSWORD)
                .field("grant_type", "password")
                .asJson();
        
        JSONObject answer = response.getBody().getObject();
        accessToken = (String) answer.get("access_token");
        
        switch (method) {
        case "POST":
        	return Unirest.post(url)
                    .header("accept", acceptType)
                    .header("content-type", contentType)
                    .header("Authorization", "Bearer " + accessToken)
                    .body(requestBody)
                    .asString();
        case "GET":
        	return Unirest.get(url)
                    .header("accept", acceptType)
                    .header("Authorization", "Bearer " + accessToken)
                    .asString();
        case "PUT":
        	return Unirest.put(url)
                    .header("accept", acceptType)
                    .header("content-type", contentType)
                    .header("Authorization", "Bearer " + accessToken)
                    .body(requestBody)
                    .asString();
        case "DELETE":
        	return Unirest.delete(url)
                    .header("accept", acceptType)
                    .header("content-type", contentType)
                    .header("Authorization", "Bearer " + accessToken)
                    .asString();
        }
        
        return null;
	}

}
