package com.company.security.interceptor;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.company.security.RequiredResourceAuthorization;
import com.company.security.config.AdditionalAuthInfo;

@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
    private AuthorizationServerTokenServices tokenService;
	
	private final AccessDeniedHandler accessDeniedHandler = new AccessDeniedHandlerImpl();
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, AccessDeniedException, IOException {
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		
		String method = request.getMethod();
		String uriWithoutContextPath = request.getRequestURI().substring(request.getContextPath().length() + 1);
		String resourceName = uriWithoutContextPath; 
		if (uriWithoutContextPath.indexOf("/") > -1) {
			resourceName = uriWithoutContextPath.substring(0, uriWithoutContextPath.indexOf("/"));
		}
		
		// Fill in aditional checks
		
		return true;
	}
}
