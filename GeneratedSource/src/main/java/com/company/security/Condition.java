package com.company.security;

import com.company.common.enums.FilterCostants;

public class Condition {

    public static final String FILTER_COMPONENTS_SEPARATOR = ",";
    public static final String FILTER_SEPARATOR = ";";
    public static final String EQUALS = "eq";
    public static final String NOT_EQUALS = "ne";
    

    private String fieldName;
    private String operator;
    private String expectedValue;

    public Condition(String filter) {
        String tokenFilter[] = filter.split(FILTER_COMPONENTS_SEPARATOR);
        this.fieldName = tokenFilter[FilterCostants.FIELDNAME.ordinal()];
        this.operator = tokenFilter[FilterCostants.OPERATOR.ordinal()];
        if (tokenFilter.length > 2) {
            this.expectedValue = tokenFilter[FilterCostants.VALUE.ordinal()];
        } else  {
            this.expectedValue = "";
        }
    }

    public Condition(String fieldName, String operator, String expectedValue,
            Class<?> metaModelClass) {
        this.fieldName = fieldName.trim();
        this.operator = operator.trim();
        this.expectedValue = expectedValue.trim();
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getOperator() {
        return operator;
    }

    public String getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(String value) {
        this.expectedValue = value;
    }
    
    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        if (fieldName != null)
            strBuilder.append(fieldName);
        strBuilder.append(FILTER_COMPONENTS_SEPARATOR);
        if (operator != null)
            strBuilder.append(operator);
        strBuilder.append(FILTER_COMPONENTS_SEPARATOR);
        if (expectedValue != null)
            strBuilder.append(expectedValue);
        
        return strBuilder.toString();
    }
}
