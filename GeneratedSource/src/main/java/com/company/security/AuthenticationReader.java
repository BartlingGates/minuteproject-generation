package com.company.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(prefix = "company.rest", name = "enabled", matchIfMissing = true)
@Service
public class AuthenticationReader {

	public boolean isSuperUser(){
		return true;
	}
	
	public Long getClientId(){
		return 0L;
	}
	
	public Long getPortalUserId(){
		return 0L;
	}
}
