package util;

import static org.junit.Assert.assertEquals;

import org.springframework.http.HttpStatus;

import com.mashape.unirest.http.HttpResponse;

public class HttpResponseChecker {

	public static void checkOk(HttpResponse<?> response) {
		check(HttpStatus.OK, response);
	}

	public static void checkCreated(HttpResponse<?> response) {
		check(HttpStatus.CREATED, response);
	}

	public static void checkBadRequest(HttpResponse<?> response) {
		check(HttpStatus.BAD_REQUEST, response);
	}

	public static void checkForbidden(HttpResponse<?> response) {
		check(HttpStatus.FORBIDDEN, response);
	}

	public static void checkNotFound(HttpResponse<?> response) {
	    check(HttpStatus.NOT_FOUND, response);
	}

	public static void checkInternalServerError(HttpResponse<?> response) {
	    check(HttpStatus.INTERNAL_SERVER_ERROR, response);
	}

	private static void check(HttpStatus expectedHttpStatus, HttpResponse<?> response) {
		String errorMsg = String.format("statusText=%s, body=%s", response.getStatusText(), response.getBody());
		assertEquals(errorMsg, expectedHttpStatus.value(), response.getStatus());
	}

}
