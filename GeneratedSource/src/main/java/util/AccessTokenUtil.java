package util;

import static util.HttpResponseChecker.checkOk;
import static util.TestConfig.AUTHENTICATION_URL;
import static util.TestConfig.PASSWORD;
import static util.TestConfig.USERNAME;

import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

public class AccessTokenUtil {

	public static String getDefaultAccessToken() throws Exception {
		return getAccessToken(USERNAME, PASSWORD);
	}
	
	public static Long getDefaultPortalUserId() throws Exception {
	    return getPortalUserId(USERNAME, PASSWORD);
	}

	public static String getAccessToken(String username, String password) throws Exception {
		HttpResponse<JsonNode> response = UnirestUtil.postRequest(AUTHENTICATION_URL, username, password);
		checkOk(response);
		JSONObject answer = response.getBody().getObject();
		String accessToken = answer.get("access_token").toString();
		return accessToken;
	}
	
	public static Long getPortalUserId(String username, String password) throws Exception {
	    HttpResponse<JsonNode> response = UnirestUtil.postRequest(AUTHENTICATION_URL, username, password);
	    checkOk(response);
	    JSONObject answer = response.getBody().getObject();
	    Long portalUserId = new Long(answer.get("portal_user_id").toString());
	    return portalUserId;
	}
}
