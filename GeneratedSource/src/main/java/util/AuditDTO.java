package util;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE)
public class AuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long clientId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;  
    
	public Long getId() {
		return id;
	}
	public Long getClientId() {
		return clientId;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
     
}
