package util;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.mashape.unirest.http.JsonNode;

public enum JsonConverterTestUtil {

	INSTANCE;

    private ObjectMapper om;

    private JsonConverterTestUtil(){
        om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
		om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		om.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		om.setSerializationInclusion(Include.NON_EMPTY);
		om.setDateFormat(StdDateFormat.getISO8601Format(StdDateFormat.getDefaultTimeZone(), Locale.getDefault()));
		om.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, true);
		om.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    public static JSONObject asJson(Object bean) throws JSONException, JsonProcessingException {
        return new JSONObject(INSTANCE.om.writeValueAsString(bean));
    }

    public static JSONArray asJsonArray(Object bean) throws  JSONException, JsonProcessingException {
        return new JSONArray(INSTANCE.om.writeValueAsString(bean));
    }

    @SuppressWarnings("unchecked")
	public static <T> T convert(String jsonString, Class<?> valueType) throws Exception {
    	Object o = INSTANCE.om.readValue(jsonString, valueType);
    	return (T) o;
    }
	public static <T> T convert(JsonNode jsonNode, Class<?> valueType) throws Exception {
    	return convert(jsonNode.toString(), valueType);
    }

}
