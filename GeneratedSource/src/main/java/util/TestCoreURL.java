package util;

import static util.TestUrlBuilder.buildCoreUrl;

import java.io.Serializable;

import com.company.model.rest.AddressResource;
import com.company.model.rest.ContactDataResource;
import com.company.model.rest.CustomerResource;
import com.company.model.rest.PortalUserPasswordResource;
import com.company.model.rest.PortalUserResource;

public enum TestCoreURL implements Serializable {
	ADDRESS(buildCoreUrl(AddressResource.REQUEST_MAPPING_VALUE)),
    CONTACT_DATA(buildCoreUrl(ContactDataResource.REQUEST_MAPPING_VALUE)),
    CUSTOMER(buildCoreUrl(CustomerResource.REQUEST_MAPPING_VALUE)),
    PORTAL_USER(buildCoreUrl(PortalUserResource.REQUEST_MAPPING_VALUE)),
    PORTAL_USER_PASSWORD_RESET(buildCoreUrl(PortalUserPasswordResource.REQUEST_MAPPING_VALUE));

    private final String url;

    private TestCoreURL(String s) {
        url = s;
    }

    public final String toString(){
        return url;
    }
}