package util;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static util.TestUrlBuilder.buildUrl;

import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

public class UnirestUtil {

//	static final String AUTHENTICATION_URL = "https://webservice-azure.company.de:9999/oauth/token";
	static final String AUTHENTICATION_URL = "https://localhost:8085/oauth/token";
    static final String OAUTH_CLIENT_ID_SECRET = "dnNtOnZzbXNlY3JldA==";

	static {
		try {
			init();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void init() throws Exception {
		CloseableHttpClient httpClient = HttpClientFactory.buildHttpClientTrustAllCertificates();
		Unirest.setHttpClient(httpClient);
	}

	public static GetRequest getRequest(Object url) throws Exception {
		return Unirest.get(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON);
	}
	
	public static GetRequest getRequest(String accessToken, Object url) throws Exception {
		return Unirest.get(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.header("Authorization", "Bearer " + accessToken);
	}

	public static HttpRequestWithBody postRequest(String accessToken, Object url) throws Exception {
		return Unirest.post(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.header("Authorization", "Bearer " + accessToken);
	}

	public static HttpRequestWithBody postRequestWithoutContentType(String accessToken, Object url) throws Exception {
		return Unirest.post(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("Authorization", "Bearer " + accessToken);
	}

	public static HttpResponse<String> postRequest(Object url, JSONObject body) throws Exception {
		return Unirest.post(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.body(body.toString()) //
				.asString();
	}
	
	public static HttpResponse<String> postRequestWithoutAuthentication(String url, JSONObject body) throws Exception {
        return Unirest.post(buildUrl(url)) //
                .header("accept", APPLICATION_JSON) //
                .header("content-type", APPLICATION_JSON) //
                .body(body.toString()) //
                .asString();
    }
	
	public static HttpResponse<JsonNode> postRequest(String url, String username, String password) throws Exception {
		return Unirest.post(buildUrl(url)) //
				.header("Authorization", "Basic " + OAUTH_CLIENT_ID_SECRET) //
				.field("username", username) //
				.field("password", password) //
				.field("grant_type", "password") //
				.asJson();
	}
	
	public static HttpResponse<String> get(String url) throws Exception {
		HttpResponse<String> response = getRequest(url).asString();
		return response;
	}

	public static HttpResponse<JsonNode> get(String accessToken, Object url) throws Exception {
		HttpResponse<JsonNode> response = getRequest(accessToken, url).asJson();
		return response;
	}

	public static HttpResponse<JsonNode> get(String accessToken, String url) throws Exception {
		HttpResponse<JsonNode> response = getRequest(accessToken, url).asJson();
		return response;
	}

	public static HttpResponse<JsonNode> put(String accessToken, Object url, JSONObject body) throws Exception {
		return putRequest(accessToken, url, body).asJson();
	}

	public static RequestBodyEntity putRequest(String accessToken, Object url, JSONObject body) throws Exception {
		RequestBodyEntity requestBodyEntity = Unirest.put(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.header("Authorization", "Bearer " + accessToken) //
				.body(body);
		return requestBodyEntity;
	}
	
	public static HttpResponse<JsonNode> putRequest(String accessToken, Object url) throws Exception {
        return Unirest.put(buildUrl(url)) //
                .header("accept", APPLICATION_JSON) //
                .header("Authorization", "Bearer " + accessToken) //
                .asJson();
    }

	public static HttpResponse<JsonNode> delete(String accessToken, Object url) throws Exception {
		HttpResponse<JsonNode> response = Unirest.delete(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.header("Authorization", "Bearer " + accessToken) //
				.asJson();
		return response;
	}

	public static HttpResponse<String> put(Object url, JSONObject body) throws Exception {
		return Unirest.put(buildUrl(url)) //
				.header("accept", APPLICATION_JSON) //
				.header("content-type", APPLICATION_JSON) //
				.body(body.toString()) //
				.asString();
	}

	public static Long getClientId(String username, String password) throws Exception {
		HttpResponse<JsonNode> response = Unirest.post(AUTHENTICATION_URL) //
				.header("Authorization", "Basic " + OAUTH_CLIENT_ID_SECRET) //
				.field("username", username) //
				.field("password", password) //
				.field("grant_type", "password") //
				.asJson();
		JSONObject answer = response.getBody().getObject();
		return ((Integer) answer.get("client_id")).longValue();
	}

}
