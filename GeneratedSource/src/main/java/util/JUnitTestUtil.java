package util;

import java.time.LocalDate;

import org.junit.Assume;

public class JUnitTestUtil {

	public static void skipUntil(int year, int month, int dayOfMonth) {
		LocalDate until = LocalDate.of(year, month, dayOfMonth);
		Assume.assumeTrue("skipping test until "+until, until.isAfter(LocalDate.now()));
	}

}
