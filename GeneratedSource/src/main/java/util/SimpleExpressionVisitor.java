package util;

import java.lang.reflect.Field;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.company.model.domain.ApplicationCode;

import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.*;
import net.sf.jsqlparser.statement.select.*;

public class SimpleExpressionVisitor implements ExpressionVisitor {

	private final static Logger LOG = Logger.getLogger(SimpleExpressionVisitor.class);

	private String normalizedExpression = "";

	private Map<String, Object> valuesMap = new TreeMap<>();

	private int i = 0;

	public String getNormalizedExpression() {
		return normalizedExpression;
	}

	public Map<String, Object> getValuesMap() {
		return valuesMap;
	}

	@Override
	public void visit(NotExpression paramNotExpression) {
		LOG.debug("visit NotExpression");
	}

	@Override
	public void visit(DateTimeLiteralExpression paramDateTimeLiteralExpression) {
		LOG.debug("visit DateTimeLiteralExpression");
	}

	@Override
	public void visit(TimeKeyExpression paramTimeKeyExpression) {
		LOG.debug("visit TimeKeyExpression");
	}

	@Override
	public void visit(OracleHint paramOracleHint) {
		LOG.debug("visit OracleHint");
	}

	@Override
	public void visit(RowConstructor paramRowConstructor) {
		LOG.debug("visit RowConstructor");
	}

	@Override
	public void visit(MySQLGroupConcat paramMySQLGroupConcat) {
		LOG.debug("visit MySQLGroupConcat");
	}

	@Override
	public void visit(KeepExpression paramKeepExpression) {
		LOG.debug("visit KeepExpression");
	}

	@Override
	public void visit(NumericBind paramNumericBind) {
		LOG.debug("visit NumericBind");
	}

	@Override
	public void visit(UserVariable paramUserVariable) {
		LOG.debug("visit UserVariable");
	}

	@Override
	public void visit(RegExpMySQLOperator paramRegExpMySQLOperator) {
		LOG.debug("visit RegExpMySQLOperator");
	}

	@Override
	public void visit(JsonOperator paramJsonOperator) {
		LOG.debug("visit JsonOperator");
	}

	@Override
	public void visit(JsonExpression paramJsonExpression) {
		LOG.debug("visit JsonExpression");
	}

	@Override
	public void visit(RegExpMatchOperator paramRegExpMatchOperator) {
		LOG.debug("visit RegExpMatchOperator");
	}

	@Override
	public void visit(OracleHierarchicalExpression paramOracleHierarchicalExpression) {
		LOG.debug("visit OracleHierarchicalExpression");
	}

	@Override
	public void visit(IntervalExpression paramIntervalExpression) {
		LOG.debug("visit IntervalExpression");
	}

	@Override
	public void visit(ExtractExpression paramExtractExpression) {
		LOG.debug("visit ExtractExpression");
	}

	@Override
	public void visit(WithinGroupExpression paramWithinGroupExpression) {
		LOG.debug("visit WithinGroupExpression");
	}

	@Override
	public void visit(AnalyticExpression paramAnalyticExpression) {
		LOG.debug("visit AnalyticExpression");
	}

	@Override
	public void visit(Modulo paramModulo) {
		LOG.debug("visit Modulo");
	}

	@Override
	public void visit(CastExpression paramCastExpression) {
		LOG.debug("visit CastExpression");
	}

	@Override
	public void visit(BitwiseXor paramBitwiseXor) {
		LOG.debug("visit BitwiseXor");
	}

	@Override
	public void visit(BitwiseOr paramBitwiseOr) {
		LOG.debug("visit BitwiseOr");
	}

	@Override
	public void visit(BitwiseAnd paramBitwiseAnd) {
		LOG.debug("visit BitwiseAnd");
	}

	@Override
	public void visit(Matches paramMatches) {
		LOG.debug("visit Matches");
	}

	@Override
	public void visit(Concat paramConcat) {
		LOG.debug("visit Concat");
	}

	@Override
	public void visit(AnyComparisonExpression paramAnyComparisonExpression) {
		LOG.debug("visit AnyComparisonExpression");
	}

	@Override
	public void visit(AllComparisonExpression paramAllComparisonExpression) {
		LOG.debug("visit AllComparisonExpression");
	}

	@Override
	public void visit(ExistsExpression paramExistsExpression) {
		LOG.debug("visit ExistsExpression");
	}

	@Override
	public void visit(WhenClause paramWhenClause) {
		LOG.debug("visit WhenClause");
	}

	@Override
	public void visit(CaseExpression paramCaseExpression) {
		LOG.debug("visit CaseExpression");
	}

	@Override
	public void visit(SubSelect paramSubSelect) {
		LOG.debug("visit SubSelect");
	}

	@Override
	public void visit(Column paramColumn) {
		LOG.debug("visit Column");
		String fullyQualifiedName = paramColumn.getFullyQualifiedName();
		String[] packageAndName = fullyQualifiedName.split("\\.");
		try {
			String classname = packageAndName[0].substring(0,1).toUpperCase() + packageAndName[0].substring(1);
			Class<?> clazz = Class.forName(ApplicationCode.class.getPackage().getName() + "." + classname);
			Field field = clazz.getDeclaredField(packageAndName[1]);
			javax.persistence.Column annotation = field.getAnnotation(javax.persistence.Column.class);
			String name = annotation.name();
			normalizedExpression += classname + "." + name;
		} catch (Exception e) {
			throw new IllegalArgumentException("paramColumn=" + paramColumn, e);
		}
	}

	@Override
	public void visit(NotEqualsTo paramNotEqualsTo) {
		LOG.debug("visit NotEqualsTo");
		paramNotEqualsTo.getLeftExpression().accept(this);
		normalizedExpression += " != ";
		paramNotEqualsTo.getRightExpression().accept(this);
	}

	@Override
	public void visit(MinorThanEquals paramMinorThanEquals) {
		LOG.debug("visit MinorThanEquals");
		paramMinorThanEquals.getLeftExpression().accept(this);
		normalizedExpression += " <= ";
		paramMinorThanEquals.getRightExpression().accept(this);
	}

	@Override
	public void visit(MinorThan paramMinorThan) {
		LOG.debug("visit MinorThan");
		paramMinorThan.getLeftExpression().accept(this);
		normalizedExpression += " < ";
		paramMinorThan.getRightExpression().accept(this);
	}

	@Override
	public void visit(LikeExpression paramLikeExpression) {
		paramLikeExpression.getLeftExpression().accept(this);
		normalizedExpression += " LIKE ";
		paramLikeExpression.getRightExpression().accept(this);
	}

	@Override
	public void visit(IsNullExpression paramIsNullExpression) {
		LOG.debug("visit IsNullExpression");
	}

	@Override
	public void visit(InExpression paramInExpression) {
		LOG.debug("visit InExpression");
	}

	@Override
	public void visit(GreaterThanEquals paramGreaterThanEquals) {
		LOG.debug("visit GreaterThanEquals");
		paramGreaterThanEquals.getLeftExpression().accept(this);
		normalizedExpression += " >= ";
		paramGreaterThanEquals.getRightExpression().accept(this);
	}

	@Override
	public void visit(GreaterThan paramGreaterThan) {
		LOG.debug("visit GreaterThan");
		paramGreaterThan.getLeftExpression().accept(this);
		normalizedExpression += " > ";
		paramGreaterThan.getRightExpression().accept(this);
	}

	@Override
	public void visit(EqualsTo paramEqualsTo) {
		LOG.debug("visit EqualsTo");
		paramEqualsTo.getLeftExpression().accept(this);
		normalizedExpression += " = ";
		paramEqualsTo.getRightExpression().accept(this);
	}

	@Override
	public void visit(Between paramBetween) {
		LOG.debug("visit Between");
	}

	@Override
	public void visit(OrExpression paramOrExpression) {
		LOG.debug("visit OrExpression");
		paramOrExpression.getLeftExpression().accept(this);
		normalizedExpression += " OR ";
		paramOrExpression.getRightExpression().accept(this);
	}

	@Override
	public void visit(AndExpression paramAndExpression) {
		LOG.debug("visit AndExpression " + paramAndExpression.getStringExpression() );
		paramAndExpression.getLeftExpression().accept(this);
		normalizedExpression += " AND ";
		paramAndExpression.getRightExpression().accept(this);
	}

	@Override
	public void visit(Subtraction paramSubtraction) {
		LOG.debug("visit Subtraction");
	}

	@Override
	public void visit(Multiplication paramMultiplication) {
		LOG.debug("visit Multiplication");
	}

	@Override
	public void visit(Division paramDivision) {
		LOG.debug("visit Division");
	}

	@Override
	public void visit(Addition paramAddition) {
		LOG.debug("visit Addition");
	}

	@Override
	public void visit(StringValue paramStringValue) {
		LOG.debug("visit StringValue store value and set variable" + paramStringValue.getValue());
		String v = "stringVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramStringValue.getValue());
	}

	@Override
	public void visit(Parenthesis paramParenthesis) {
		LOG.debug("visit Parenthesis");
		normalizedExpression += " ( ";
		paramParenthesis.getExpression().accept(this);
		normalizedExpression += " ) ";
	}

	@Override
	public void visit(TimestampValue paramTimestampValue) {
		LOG.debug("visit TimestampValue store value and set variable" + paramTimestampValue.getValue());
		String v = "timestampVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramTimestampValue.getValue());
	}

	@Override
	public void visit(TimeValue paramTimeValue) {
		LOG.debug("visit TimeValue store value and set variable" + paramTimeValue.getValue());
		String v = "timeVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramTimeValue.getValue());
	}

	@Override
	public void visit(DateValue paramDateValue) {
		LOG.debug("visit DateValue store value and set variable" + paramDateValue.getValue());
		String v = "dateVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramDateValue.getValue());
	}

	@Override
	public void visit(HexValue paramHexValue) {
		LOG.debug("visit HexValue");
	}

	@Override
	public void visit(LongValue paramLongValue) {
		LOG.debug("visit LongValue store value and set variable" + paramLongValue.getValue());
		String v = "longVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramLongValue.getValue());
	}

	@Override
	public void visit(DoubleValue paramDoubleValue) {
		LOG.debug("visit DoubleValue store value and set variable" + paramDoubleValue.getValue());
		String v = "doubleVariable"+i++;
		normalizedExpression += ":"+v;
		valuesMap.put(v, paramDoubleValue.getValue());
	}

	@Override
	public void visit(JdbcNamedParameter paramJdbcNamedParameter) {
		LOG.debug("visit JdbcNamedParameter");
	}

	@Override
	public void visit(JdbcParameter paramJdbcParameter) {
		LOG.debug("visit JdbcParameter");
	}

	@Override
	public void visit(SignedExpression paramSignedExpression) {
		LOG.debug("visit SignedExpression");
		normalizedExpression += paramSignedExpression.getSign();
		paramSignedExpression.getExpression().accept(this);
	}

	@Override
	public void visit(Function paramFunction) {
		LOG.debug("visit Function");
	}

	@Override
	public void visit(NullValue paramNullValue) {
		LOG.debug("visit NullValue");
	}
}
