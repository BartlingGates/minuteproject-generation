package util;

import org.apache.commons.lang.StringUtils;

public class TestUrlBuilder {
	
	private static final String PATH_SEPARATOR = "/";
	
    private static final String DEFAULT_PROTOCOL = "https://";

    private static final String DEFAULT_CORE_PREFIX = DEFAULT_PROTOCOL+"127.0.0.1:8085";
//    private static final String DEFAULT_CORE_PREFIX = DEFAULT_PROTOCOL+"51.4.225.248:8090";

    private static final String DEFAULT_PREFIX = DEFAULT_CORE_PREFIX+PATH_SEPARATOR+"company";


    public final static String buildUrl(Object postfixObject, Object...append) {
    	return buildUrl(DEFAULT_PREFIX, postfixObject) + StringUtils.join(append, PATH_SEPARATOR);
    }

    public final static String buildCoreUrl(Object postfixObject, Object...append) {
    	return buildUrl(DEFAULT_CORE_PREFIX, postfixObject) + StringUtils.join(append, PATH_SEPARATOR);
    }

	private static String buildUrl(String defaultPrefix, Object postfixObject) {
		String postfix = postfixObject.toString();
    	if (postfix.startsWith(DEFAULT_PROTOCOL)) {
    		return postfix;
    	}
    	if (!postfix.startsWith(PATH_SEPARATOR)) {
    		postfix = PATH_SEPARATOR+postfix;
    	}
    	return defaultPrefix+postfix;
	}
}