package util;

import java.util.ArrayList;
import java.util.List;

import com.company.query.filter.FilterCriteria;

public class FilterCriteriaFactory {
    
    private FilterCriteriaFactory() {
    }
    
    public static String getLinkingOperator(String filter) {
        if (filter != null && filter.toUpperCase().startsWith(FilterCriteria.LINKING_OPERATOR_OR + FilterCriteria.LINKING_OPERATOR_SEPERATOR)) {
            return FilterCriteria.LINKING_OPERATOR_OR;
        }
        return FilterCriteria.LINKING_OPERATOR_AND;
    }
    
    public static List<FilterCriteria> getFilterCriterias(String filter, Class<?> staticMetaModelClass) {
        List<FilterCriteria> filterCriterias = new ArrayList<>();
        filter = getFilterWithoutLinkingOperator(filter);
        if (filter != null) {
            String[] token = filter.split(FilterCriteria.FILTER_SEPARATOR);
            for (String str : token) {
                filterCriterias.add(new FilterCriteria(str, staticMetaModelClass));
            }
        }
        
        return filterCriterias;
    }
    
    private static String getFilterWithoutLinkingOperator(String filter) {
        if (filter != null) {
            if (filter.toUpperCase().startsWith(FilterCriteria.LINKING_OPERATOR_OR + FilterCriteria.LINKING_OPERATOR_SEPERATOR))
                filter = filter.substring((FilterCriteria.LINKING_OPERATOR_OR + FilterCriteria.LINKING_OPERATOR_SEPERATOR).length());
            
            if (filter.toUpperCase().startsWith(FilterCriteria.LINKING_OPERATOR_AND + FilterCriteria.LINKING_OPERATOR_SEPERATOR)) 
                filter = filter.substring((FilterCriteria.LINKING_OPERATOR_AND + FilterCriteria.LINKING_OPERATOR_SEPERATOR).length());
        }
        return filter;
    }
    
    public static List<FilterCriteria> getFilterCriteria(String filter, Class<?> clazz, FilterCriteria initialFilterCriteria) {
        List<FilterCriteria> filterCriterias = new ArrayList<>();
        
        if(initialFilterCriteria != null){
            filterCriterias.add(initialFilterCriteria);
        }
        
        if (filter != null) {
            filter = getFilterWithoutLinkingOperator(filter);
            String[] token = filter.split(FilterCriteria.FILTER_SEPARATOR);
            for (String str : token) {
                filterCriterias.add(new FilterCriteria(str, clazz));
            }
        }
        return filterCriterias;
    }

}
