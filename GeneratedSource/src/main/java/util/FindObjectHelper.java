package util;

import java.util.Set;

public class FindObjectHelper {

    public static<T> T findInSet(T source, Set<T> set) {
        for (T obj : set) {
            if (obj.equals(source)) {
                return obj;
            }
        }
        return null;
    }
    
    /**
     * Prüft case insensitive, ob ein String sich in der Liste befindet
     * @param list die Liste mit Strings
     * @param str der String, der geprüft werden soll
     * @return true, wenn es ein case insensitives Match gab, sonst false
     */
    public static boolean containsCaseInsensitive(String[] list, String str) {
        for (String element : list) {
            if (element.equalsIgnoreCase(str)) {
                return true;
            }
        }
        
        return false;
    }
}
