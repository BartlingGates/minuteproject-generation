package util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class PageableFactory {
	public static  Pageable createPageable(Integer page, Integer size, String sort) {
        List<String> sortList = new ArrayList<>();
        

        int pagenumber = page != null ? page : 0;
        int pagesize = size != null ? size : Integer.MAX_VALUE;
        String sortfield = "id";
        Direction direction = Direction.ASC;
        
        if(sort != null) {
            // Do not apply any default sort.. -> implemented for distance sorting in SQL/HQL
            if(sort.equalsIgnoreCase("NO_DEFAULT_ID_SORT")) {
            	return new PageRequest(pagenumber, pagesize, null);
            	
            } else if(sort.startsWith("+")) {
                sort = sort.substring(1, sort.length());
                direction = Direction.ASC;
                sortfield = sort;
            } else if(sort.startsWith("-")) {
                sort = sort.substring(1, sort.length());
                direction = Direction.DESC;
                sortfield = sort;
            }
        }
        
        sortList.add(sortfield + " " + direction);
        
        Pageable pageable = new PageRequest(
            pagenumber, pagesize, new Sort(direction, sortfield)
        );

        return pageable;
    }
}
