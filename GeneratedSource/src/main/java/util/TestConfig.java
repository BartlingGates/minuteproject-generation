package util;


public class TestConfig {
    public static final int NUM_ENTITIES = 2;  // Number of imported test documents
    public static final int NUM_ENTITIES_MEDIUM = 5;  // Number of imported test documents
    public static final int NUM_ENTITIES_LARGE = 10;  // Number of imported test documents
    public static final int PERCENTAGE = 50;  // Probability of Change
    public static final int MAX_SHEETS = 30;  // Maximal number of sheets per document

    public static final int FIRST_SHEET = 1;
    public static final int SECOND_SHEET = 2;

    public static final int UPPER_RND_BOUND = 100;
    
    public static final int NUM_RELATED_ENTITIES = 10;
    
    // TODO move this to application.properties
    
    public static final String AUTHENTICATION_URL = "https://localhost:8085/oauth/token";
    public static final String OAUTH_CLIENT_ID_SECRET = "dnNtOnZzbXNlY3JldA==";
    public static final String USERNAME = "company";
    public static final String PASSWORD = "$2a$10$MhbIdAfB3SyiBZxsOGHTUuCVCVuH8QMtHkU.xiEZlQY1cQI9FhQYm";
    
    public static final String MAIL_ADDRESS = "test@example.com";
    
    public static final Long CLIENT_ID = Long.valueOf(0);
    public static final Long CLIENT_ID_MB = Long.valueOf(555);
    public static final Long CLIENT_ID_SP = Long.valueOf(666);
    
}
