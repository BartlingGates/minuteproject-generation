package util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import net.sf.jsqlparser.statement.StatementVisitorAdapter;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.OrderByVisitorAdapter;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectVisitorAdapter;

public class SimpleStatementVisitor extends StatementVisitorAdapter {

	private final static Logger LOG = Logger.getLogger(SimpleStatementVisitor.class);

	List<String> orderByList = new ArrayList<>();

	public List<String> getOrderByList() {
		return orderByList;
	}

	public void visit(Select select) {
		LOG.debug("visit Select");
		select.getSelectBody().accept(new SelectVisitorAdapter() {
			public void visit(PlainSelect plainSelect) {
				LOG.debug("visit PlainSelect");
				List<OrderByElement> orderByElements = plainSelect.getOrderByElements();
				for (OrderByElement orderByElement : orderByElements) {
					orderByElement.accept(new OrderByVisitorAdapter() {
						public void visit(OrderByElement orderBy) {
							LOG.debug("visit OrderByElement");
							String s = "";
							SimpleExpressionVisitor expressionVisitor = new SimpleExpressionVisitor();
							orderBy.getExpression().accept(expressionVisitor);
							s += expressionVisitor.getNormalizedExpression();
							if (orderBy.isAsc()) {
								s += " ASC";
							} else {
								s += " DESC";
							}
							orderByList.add(s);
						}
					});
				}
			}
		});
	}

}
