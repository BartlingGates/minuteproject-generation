package util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.JoinColumn;

import io.github.benas.jpopulator.api.Populator;
import io.github.benas.jpopulator.impl.PopulatorBuilder;

public enum TestObjectFactory {

	INSTANCE;

	private final static String[] DEFAULT_EXCLUDE_FIELDS = {"parentType"};

	private Populator populator;

	private TestObjectFactory() {
		populator = new PopulatorBuilder().build();
	}

	public static TestObjectFactory getInstance() {
		return INSTANCE;
	}

	public static <T> List<T> build(int numberOfEntities, Class<T> clazz, String...excludeFields) {
		return (List<T>) INSTANCE.populator.populateBeans(clazz, numberOfEntities, getFieldnamesForParentId(clazz, false, excludeFields));
	}

	public static <T> List<T> buildExcludeSets(int numberOfEntities, Class<T> clazz, String...excludeFields) {
		return (List<T>) INSTANCE.populator.populateBeans(clazz, numberOfEntities, getFieldnamesForParentId(clazz, true, excludeFields));
	}

	public static <T> T build(Class<T> clazz, String... excludeFields) {
		return build(1, clazz, excludeFields).get(0);
	}

	public static <T> T buildExcludeSets(Class<T> clazz, String... excludeFields) {
		return buildExcludeSets(1, clazz, excludeFields).get(0);
	}

	private static String[] getFieldnamesForParentId(Class<?> clazz, boolean excludeSets, String... excludeFields) {
		List<String> fieldnames = new ArrayList<>();
		for (Field f : clazz.getDeclaredFields()) {

			boolean isNullable = clazz.getSimpleName().endsWith("DTO")
					|| f.getAnnotation(JoinColumn.class) != null //
					|| (f.getAnnotation(Column.class) != null && f.getAnnotation(Column.class).nullable());

			if (isNullable &&
					f.getName().endsWith("Id")) {
				fieldnames.add(f.getName());
			}
			if (f.getName().endsWith("_contractNumber")) {
				fieldnames.add(f.getName());
			}
			if (f.getName().endsWith("Id_")) {
				fieldnames.add(f.getName());
			}
			if (f.getName().equals("id")) {
				fieldnames.add(f.getName());
			}
			if (excludeSets && f.getType().isAssignableFrom(Set.class)) {
				fieldnames.add(f.getName());
			}

		}
		if (excludeFields != null) {
			fieldnames.addAll(Arrays.asList(excludeFields));
		}
		fieldnames.addAll(Arrays.asList(DEFAULT_EXCLUDE_FIELDS));
		return fieldnames.toArray(new String[0]);
	}

}
