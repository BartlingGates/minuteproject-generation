package util;

public class ClientIdGenerator {

	private static Long clientId = 1000L;
	
	public static Long nextClientId() {
		return clientId++;
	}
	
}
